select SO1.ID
, SO1.name as [NomeTabella]
, SO1.xtype as [TipoTabella]
, SCL.name as [NomeColonna]
, SCL.xtype as [TipoColonna]
, SCL.length as [ByteColonna]
, SCL.prec as [PrecisioneColonna]
, SCL.scale as [ScalaColonna]
, SCL.colid as [IdColonna]
, cast(SCL.isnullable as bit) as isNullable
, SCL.iscomputed
, SI.name as [NomeIndice]
, SI.status as [StatoIndice]
, SI.keycnt as [NumeroChiavi]
, CASE WHEN ((SI.status & 2048)=2048) THEN CAST(1 AS BIT) ELSE CAST(0 AS BIT) END as [isPrimary]
, CASE WHEN ((SI.status & 2)=2) THEN CAST(1 AS BIT) ELSE CAST(0 AS BIT) END as [isUnique]
, SIK.indid as [IDIndice]
, SIK.colid
, SK.status as [StatusConstraint]
, CASE WHEN ((SK.status & 3)=3) THEN CAST(1 AS BIT) ELSE CAST(0 AS BIT) END AS [isForeignKey]
, CASE WHEN ((SK.status & 5)=5) THEN CAST(1 AS BIT) ELSE CAST(0 AS BIT) END AS [hasDefault]
, CASE WHEN ((SCL.status & 128)=128) THEN CAST(1 AS BIT) ELSE CAST(0 AS BIT) END AS [isIdentity] 
from sysobjects SO1 
INNER JOIN syscolumns SCL ON SCL.id=SO1.id 
LEFT JOIN sysindexkeys SIK ON SIK.ID=SO1.ID and SIK.colid=SCL.colid 
LEFT JOIN sysindexes SI ON SI.id=SCL.id and SI.indid=SIK.indid 
LEFT JOIN sysconstraints SK ON SK.id=SIK.id AND SK.colid=SIK.colid 
LEFT JOIN sysusers SU ON SO1.uid=SU.uid
WHERE SO1.xtype='U' AND SO1.name='attivita' 
AND SU.name='dbo'
ORDER BY SCL.colorder