﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.Odbc;

namespace simplex_ORM
{
    /// <summary>
    /// Classe astratta che rappresenta il catalogo ad un database.
    /// Deve essere implementata una classe di questo tipo per ogni tecnologia Database.
    /// @MdN 10-01-2012
    /// </summary>
   public abstract class Catalog
    {
        #region ATTRIBUTI STATICI
        ///// <summary>
        ///// Elenco dei tipi gestiti dal database.
        ///// </summary>
        // static String _Types; //<deleted @MdN: 25/01/2020 />



        #endregion

        #region ATTRIBUTI PRIVATI E PROTETTI
        /// <summary>
        /// Connessione al database
        /// </summary>
        protected OdbcConnection l_st_connection;
        /// <summary>
        /// Nome del database che contiene i metadati
        /// </summary>
        protected String l_CataDBName;
        /// <summary>
        /// Stringa di connessione.
        /// </summary>
        protected String l_ConnectionString;
        #endregion


        #region LETTURA CATALOGO

        /// <summary>
        /// Apre una connessione al catalogo e legge le meta-informazioni.
        /// MdN: 12/12/2013
        /// </summary>
        public abstract void openCatalog();


        /// <summary>
        /// Apre una connessione al catalogo e legge le meta-informazioni.
        /// MdN: 14/01/2013
        /// </summary>
        /// <param name="Str_Connection">Stringa di connessione al database</param>
        public abstract void openCatalog(String Str_Connection);

        /// <summary>
        /// Carica i tipi gestiti dal database e crea il mappaggio.
        /// MdN: 14/01/2013
        /// </summary>
        public abstract void getTypes();

        /// <summary>
        /// Chiude il catalogo.
        /// </summary>
        public abstract void closeCatalog();

        #endregion
    }
}
