﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using simplex_ORM;

namespace simplex_ORM.SQLSrv
{
    /// <summary>
    /// <p>
    /// Cerca in una data tabella di Look-up il calore corrispondente alla chiave primaria
    /// passata come parametro.
    /// </p>
    /// <p>
    /// Non ha costruttori pubblici ed un oggetto può essere creato solo da un'istanza di 
    /// una classe SQLTable da cui prende:
    /// <ul>
    /// <li>Connessione attiva</li>
    /// <li>Nome della tabella</li>
    /// <li>Nome e tipo della chiave PRIMARIA</li>
    /// </ul>
    /// Il costruttore restituisce un messaggio di tipo simplex_ORMMessage nel caso in cui la
    /// tabella non possa essere vista come una Lookup (ad esempio in caso di chiavi composte).
    /// </p>
    /// <pre>
    /// ----
    /// @MdN
    /// Crtd: 08/08/2015
    /// </pre>
    /// </summary>
    public class SQLLookupBrowser: simplex_ORM.iLookupBrowser
    {
        /// <summary>
        /// Connessione attiva.
        /// Normalmente in stato Closed.
        /// </summary>
        internal System.Data.Odbc.OdbcConnection MyConn = null;
        /// <summary>
        /// Nome della tabella di ricerca
        /// </summary>
        internal String MyTableName = null;
        /// <summary>
        /// Chiave di ricerca
        /// </summary>
        internal String MyKey = null;
        /// <summary>
        /// Valore da restituire
        /// </summary>
        internal String MyValueColumn = null;
        /// <summary>
        /// Tipo di chiave:
        /// 0 = intero
        /// 1 = stringa
        /// </summary>
        internal int KeyType = 0; 

        /// <summary>
        /// <p>Costruttore interno.
        /// </p>
        /// 
        /// </summary>
        /// <param name="p_Key">Colonna Chiave PRIMARIA</param>
        /// <param name="p_Value">Colonna del valore</param>
        /// <param name="p_Conn">Connessione attiva. Se aperta viene chiusa.</param>
        /// <param name="p_TableName">Nome della tabella da consultare</param>
        internal SQLLookupBrowser(String p_Key, String p_Value, String p_TableName, System.Data.Odbc.OdbcConnection p_Conn)
        {
            MyConn = p_Conn;
            if (p_Conn != null && p_Conn.State == System.Data.ConnectionState.Open)
                p_Conn.Close();

            MyKey = p_Key;

            MyTableName = p_TableName;
            MyValueColumn = p_Value;
        }

        /// <summary>
        /// Restituisce il valore corrispondente alla chiave specificacta come parametro.
        /// </summary>
        /// <param name="p_Key">Chiave di cui si vuole il corrispondente valore.</param>
        /// <returns>Il valore richiesto o null.</returns>
        public String getValue(String p_Key)
        {
            // controllo
            if (p_Key == null)
                return null;
            if (MyConn == null)
                return null;

            System.Text.StringBuilder _Sql = new StringBuilder("SELECT ");
            System.Data.Odbc.OdbcCommand _cmd;
            String _toRet = null;

            _Sql.Append(this.MyValueColumn).Append(" FROM ").Append(this.MyTableName).Append(" WHERE ").Append(this.MyKey).Append(" = ");
            if (KeyType == 0)
                _Sql.Append(p_Key);
            else
                _Sql.Append("'").Append(p_Key).Append("'");

            if (MyConn.State == System.Data.ConnectionState.Open)
                MyConn.Close();

            MyConn.Open();
            _cmd = MyConn.CreateCommand();
            _cmd.CommandText = _Sql.ToString();
            try
            {
                _toRet = (String)_cmd.ExecuteScalar();
            }
            catch (Exception E)
            {
                MyConn.Close();
                throw new simplex_ORM.Spx_ORMMessage(0, "SQLLookupBrowser.getValue(): Errore nella query [" + _Sql + "]", E);
            }
            MyConn.Close();
            return _toRet;
        }

        /// <summary>
        /// Restituisce il valore corrispondente alla chiave NUMERICA specificacta come parametro.
        /// </summary>
        /// <param name="p_Key">Chiave NUMERICA di cui si vuole il corrispondente valore.</param>
        /// <returns>Il valore richiesto o null.</returns>
        public String getValue(Int32 p_Key)
        {
            return getValue(p_Key.ToString());
        }
    }
}
