﻿/*
 * LINGUE:
 * _EN: INGLESE
 * _IT: ITALIANO
 */
#define _EN
#define DEF_IT

using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Odbc;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;

namespace simplex_ORM.SQLSrv
{
    /// <summary>
    /// <p>
    /// Classe che rappresenta una tabella di un database in tecnologia SQL Server 2000 
    /// e versioni successive.
    /// </p>
    /// <b>ATTENZIONE:</b><br></br>
    /// <p>
    /// Le operazioni si salvataggio prevedono che la connessione ODBC venga aperta e chiusa
    /// dal chiamante!!!.
    /// </p>
    /// <pre>
    /// ----
    /// @MdN
    /// Crtd: ????
    /// Mdfd: 08/08/2015 - Introdotto il metodo pubblico getLookupBrowser.
    /// Mdfd: 18/08/2015 - Livorno: se viene annullato il valore di una colonna chiave IDENTITY, l'oggetto diviene pronto per un inserimento.
    /// ---- Versione 1.1.0.0.
    /// Mdfd: 21/09/2015 - Introdotta la proprietà pubblica di sola lettura (GET) PrimaryKeyColumns.
    /// ---- Versione 1.1.0.1 (simplex_ORM.dll 1.1.0.5)
    /// Mdfd: 19/05/2017 - Resa più robusta la query del metodo LoadInfo(): falliva in caso di definizione di più indici sulla medesima colonna.
    /// ---- Versione 1.1.0.2 (simplex_ORM.dll 1.1.0.6)
    /// Mdfd:   01/10/2017 - Monterotondo - introdotta property pubblica Loaded.
    ///         01/10/2017 - Monterotondo - introdotto metodo pubblico "Boolean isDecimalField(String p_columnName)"
    ///         01/10/2017 - Monterotondo - introdotto metodo pubblico "Boolean isIntegerField(String p_columnName)"
    ///         01/10/2017 - Monterotondo - introdotto metodo pubblico "Boolean isDateTimeField(String p_columnName)"
    /// ---- Versione 1.1.0.3 (simplex_ORM.dll 1.1.0.9)
    /// Mdfd:   17/07/2018 - Monterotondo - Introdotto metodo pubblico "Boolean isIdentityColumn(String p_colimnName)"
    ///                                   - Introdotto metodo pubblico "String decodeColumn(String p_ReferencingColumnName, String p_DecodedColumnName, Boolean p_ignoreException)"
    /// Mdfd:   21/07/2018 - Monterotondo - Correzione in getValue().
    /// ---- Versione 1.1.0.4 (simplex_ORM.dll 1.1.0.12)
    /// Mdfd:   09/04/2020 - Monterotondo - Correzioni in loadInfo()
    ///                                     - issue #11 Gestione delle colonne multiple. Modificare la query di consultazione del catalogo - aggiungere un'importantissima clausola di ordinamento per sicurezza.
    ///                                     - issue #11 Gestione delle colonne multiple. NON inserire una colonna già inserita.
    ///                                    - Correzioni in setValue()
    ///                                     - issue #08 SQLTable.setValue(): eseguire il troncamento automatico del valore SOLO in caso di valori stringa (VARCHAR, NVARCHAR, etc). 
    ///                                    - Altre correzioni
    ///                                     - issue #09  SQLTable().isModified(String p_TableName) metodo pubblico per verificare se un dato attributo ha subito una modifica dall'ulimo caricamento (load).
    /// </pre>
    /// </summary>
    public class SQLTable:Table
    {
        #region MESSAGGI
#if _EN
        //static String _ERR_G = "Errors. See the history of errors."; //<deleted @MdN: 25/01/2020 />
        static String _ERR = "SQLTable.createSQLTable(): errors. See the history of errors.";
        static String _ERR_0 = "SQLTable.createSQLTable(): the ODBC connection is null.";
        static String _ERR_00 = "SQLTable.createSQLTable(): table name not specified (is null).";
        static String _ERR_1 = "SQLTable.loadInfo(): Errors on loading table's informations: see inner messages for details.";
        static String _ERR_2 = "SQLTable.load(): Primary Key not fully specified.";
        static String _ERR_3 = "SQLTable.load(): abnormal column list. Exit.";
        static String _ERR_4 = "SQLTable.load(): Closed connection.;";
        static String _ERR_5 = "SQLTable.load(): Unable to open the connection with the database.";
        static String _ERR_6 = "SQLTable.load(): Error on the execution of the query";
        static String _ERR_7 = "SQLTable.load(): The specificed record does not exist.";
        static String _ERR_8 = "SQLTable.load(): Error on parsing a date in SQL String format. See inner message for details";
        static String _ERR_9 = "SQLTable.load(): Error on . See inner message for details";
        static String _ERR_10 = "SQLTable.setValue():Type mismatch error in column <%VALUE%> : NOT A VALID SQL-92 FORMAT DATE.";
        static String _ERR_11 = "SQLTable.setValue():Type mismatch error in column <%VALUE%> : NOT A VALID NUMBER.";
        static String _ERR_12 = "SQLTable.isIdentityColumn(): column <%VALUE%> not found.";
        static String _ERR_13 = "Column name not specified.";
        static String _ERR_14 = "ODBC Connection closed.";
        static String _ERR_15 = "Transaction not specified.";


#endif
#if _IT
        static String _ERR_G = "Errori. Consultare la storia degli errori.";
        static String _ERR = "SQLTable.createSQLTable(): errori. Vedi lo storico degli errori.";
        static String _ERR_12 = "SQLTable.isIdentityColumn(): La colonna <%VALUE%> non è stata trovata.";
        static String _ERR_14 = "Connessione ODBC chiusa.";
        static String _ERR_15 = "Transazione non specificata.";
#endif

        #endregion

        /// <summary>
        /// Identificatore dell'oggetto Tabella nel catalogo.
        /// </summary>
        String l_ObjectID = null;

        #region METODI PUBBLICI

        /// <summary>
        /// Esegue la decodifica di una colonna che contiene il riferimento numerico (CHIAVE) alla
        /// descrizione contenuta in una apposita tabella di LOOKUP.<br></br>
        /// 
        /// La tabella di Lookup viene determinata automaticamente da Simplex se la progettazione del database
        /// è stata correttamente realizzata con vincoli di integrità referenziale.<br></br>
        /// 
        /// <remarks>Ignora eventuali eccezioni.</remarks>
        /// <pre>
        /// ----
        /// @MdN
        /// Crtd: 14/07/2018 - Monterotondo
        /// </pre>
        /// </summary>
        /// <param name="p_ReferencingColumnName">Colonna che contiene il codice da decodificare.</param>
        /// <param name="p_DecodedColumnName">Colonna che contiene il valore decodificato prelevato dalla tabella di Lookup.</param>
        /// <returns>Il valore (Stringa) di decodifica ovvero null in caso di anomalie.</returns>
        protected String decodeColumn(String p_ReferencingColumnName, String p_DecodedColumnName)
        {
            return decodeColumn(p_ReferencingColumnName, p_DecodedColumnName, true);
        }


        /// <summary>
        /// Esegue la decodifica di una colonna che contiene il riferimento numerico (CHIAVE) alla
        /// descrizione contenuta in una apposita tabella di LOOKUP.<br></br>
        /// 
        /// La tabella di Lookup viene determinata automaticamente da Simplex se la progettazione del database
        /// è stata correttamente realizzata con vincoli di integrità referenziale.<br></br>
        /// <pre>
        /// ----
        /// @MdN
        /// Crtd: 14/07/2018 - Monterotondo
        /// </pre>
        /// </summary>
        /// <param name="p_ReferencingColumnName">Colonna che contiene il codice da decodificare.</param>
        /// <param name="p_DecodedColumnName">Colonna che contiene il valore decodificato prelevato dalla tabella di Lookup.</param>
        /// <param name="p_ignoreException">Non lancia eccezioni (true) e restituisce semplicemente il valore null.</param>
        /// <returns>Il valore (Stringa) di decodifica ovvero null in caso di anomalie.</returns>
        protected String decodeColumn(String p_ReferencingColumnName, String p_DecodedColumnName, Boolean p_ignoreException)
        {
            simplex_ORM.SQLSrv.SQLForeignKey _fk = null;
            simplex_ORM.SQLSrv.SQLTable _tbl = null;

            // * controlli primari
            if (p_ReferencingColumnName == null || p_DecodedColumnName == null)
                return null;
            if (this.Name == null)
                return null;

            // Ottengo il vincolo
            _fk = getForeignKey(p_ReferencingColumnName);

            // ** contollo secondario
            if (_fk == null)
                return null;

            _tbl = _fk.ReferencedTable;
            // ** controllo secondario
            if (_tbl == null)
                return null;

            // ** controllo secondario
            if (_fk.ReferencedColumns[0] == null)
                return null;

            // ** controllo secondario
            if (getValue(p_ReferencingColumnName) == null)
                return null;

            // caricamento della tabella
            try
            {
                _tbl.setValue(_fk.ReferencedColumns[0], getValue(p_ReferencingColumnName));
                _tbl.load();
            }
            catch (Spx_ORMMessage _ormm)
            {
                if (p_ignoreException == true)
                    return null;
                else
                {
                    _ormm.Source = "SQLTable.decodeColumn()-->" + _ormm.Source;
                    throw _ormm;
                }
            }
            catch (Exception E)
            {
                if (p_ignoreException == true)
                    return null;
                else
                {
                    simplex_ORM.Spx_ORMMessage _ormm = new Spx_ORMMessage(E);
                    _ormm.Source = "SQLTable.decodeColumn(): " + E.Source;
                    _ormm.Message = E.Message;
                    _ormm.MessageCode = 0;
                    throw _ormm;
                }
            }

            // lettura e restituzione del valore decodificato
            return getValue(p_DecodedColumnName).Trim();
        }

        /// <summary>
        /// Verifica se una data colonna è di tipo IDENTITY (true).
        /// <pre>
        /// ----
        /// @MdN
        /// Crtd: 14/07/2018
        /// </pre>
        /// </summary>
        /// <param name="p_ColumnName">Nome della colonna.</param>
        /// <returns>TRUE, se la colonna è identity.</returns>
        public Boolean isIdentityColumn(String p_ColumnName)
        {
            // * controlli primari
            if (p_ColumnName == null)
            {
                Spx_ORMMessage _ormm = new Spx_ORMMessage();
                _ormm.MessageType = (int)Spx_ORMMessage.MESSAGE_TYPE.ERROR_MSG;
                _ormm.MessageCode = 12;
                _ormm.Message = _ERR_13;
                _ormm.Source = "SQLTable.isIdentityColumn():";
                throw _ormm;
            }

            if (this.SQLColumns.Contains(p_ColumnName) == false)
            {
                Spx_ORMMessage _ormm = new Spx_ORMMessage();
                _ormm.MessageType = (int)Spx_ORMMessage.MESSAGE_TYPE.ERROR_MSG;
                _ormm.MessageCode = 12;
                _ormm.Message = _ERR_12.Replace("<%VALUE%>", p_ColumnName);
                _ormm.Source = "SQLTable.isIdentityColumn():";
                throw _ormm;
            }

            // prendo l'oggetto SQLColumn ed eseguo il test
            foreach (SQLColumn _c in l_Columns)
            {
                if (_c.Name.CompareTo(p_ColumnName) == 0)
                {
                    return _c.l_IsIdentity;
                }
            }

            // se siamo arrivati qui qualcosa non è andato come avrebbe dovuto
            //if (this.SQLColumns.Contains(p_ColumnName) == false)
            {
                Spx_ORMMessage _ormm = new Spx_ORMMessage();
                _ormm.MessageType = (int)Spx_ORMMessage.MESSAGE_TYPE.ERROR_MSG;
                _ormm.MessageCode = 12;
                _ormm.Message = _ERR_12.Replace("<%VALUE%>", p_ColumnName);
                _ormm.Source = "SQLTable.isIdentityColumn()";
                throw _ormm;
            }
        }

        /// <summary>
        /// Crea una nuova istanza di SQLTable all'interno di una transazione.
        /// ----
        /// @MdN
        /// Crtd: 18/12/2013
        /// Mdfd: 16/04/2015
        /// Mdfd: Livorno - 15/08/2015 - controllo del parametro p_Name
        /// ---- OVERLOAD CON TRANSAZIONE ----
        /// Crtd: 29/07/2018 - Monterotondo
        /// </summary>
        /// <param name="p_Connection">Connessione corrente.</param>
        /// <param name="p_Name">Nome della tabella</param>
        /// <param name="p_Trans">Transazione attiva</param>
        /// <returns>Istanza di una tabella.</returns>
        public static SQLTable createSQLTable(OdbcConnection p_Connection, String p_Name, System.Data.Odbc.OdbcTransaction p_Trans)
        {
            if (p_Connection == null)
            {
                //throw new Spx_ORMMessage("SQLTable.createSQLTable(): the ODBC connection is null.",1); //@MdN 16/04/2015 - @MdN 15/08/2015 Livorno: cambiato il codice da 3 a 1
                throw new Spx_ORMMessage(_ERR_0, 1); //@MdN 01/10/2017 Monterotondo: introdotta variabile statica errore 0 - @MdN 16/04/2015 - @MdN 15/08/2015 Livorno: cambiato il codice da 3 a 1
            }

            if (p_Trans == null)
            {
                //throw new Spx_ORMMessage("SQLTable.createSQLTable(): the ODBC connection is null.",1); //@MdN 16/04/2015 - @MdN 15/08/2015 Livorno: cambiato il codice da 3 a 1
                throw new Spx_ORMMessage(_ERR_15, 1); //@MdN 01/10/2017 Monterotondo: introdotta variabile statica errore 0 - @MdN 16/04/2015 - @MdN 15/08/2015 Livorno: cambiato il codice da 3 a 1
            }

            if (p_Connection.State == ConnectionState.Closed)
                throw new Spx_ORMMessage(_ERR_14, 1); 

            // Livorno 15/08/2015 - Monterotondo 01/10/2017
            if (p_Name == null)
            {
                //throw new Spx_ORMMessage("SQLTable.createSQLTable(): table name not specified (is null).", 1);
                throw new Spx_ORMMessage(_ERR_00, 1); //@MdN 01/10/2017 Monterotondo: introdotta variabile statica errore 0
            }
            // Livorno 15/08/2015 - Monterotondo 01/10/2017

            SQLTable toRet = new SQLTable(p_Connection, p_Name);
            try
            {
                toRet.loadInfo(p_Trans);
            }
            catch (Spx_ORMMessage EE)
            {
                EE.store();
                EE.Message = _ERR;
                EE.MessageCode = 0;
                throw EE;
            }

            #region MEV SQLTable
            //ALTRE EVENTUALI FUNZIONI VANNO INSERITE DI SEGUITO.

            // fine
            #endregion
            
            return toRet;
        }



        /// <summary>
        /// Crea una nuova istanza di SQLTable.
        /// Se la connessione passata come parametro è chiusa la apre automaticamente (added @MdN: 26/01/2020).<br></br>
        /// L'oggetto restituito ha la connessione CHIUSA. NON USARE IN UNA TRANSAZIONE. In una transazione usare lo specifico OVERLOAD.<br></br>
        /// ----
        /// @MdN
        /// Crtd: 18/12/2013
        /// Mdfd: 16/04/2015
        /// Mdfd: Livorno - 15/08/2015 - controllo del parametro p_Name 
        /// Mdfd: Monterotondo - 26/01/2020 - se la connessione associata all'oggetto è chiusa la apre. 
        /// </summary>
        /// <param name="p_Connection">Connessione corrente.</param>
        /// <param name="p_Name">Nome della tabella</param>
        /// <returns>Istanza di una tabella.</returns>
        public static SQLTable createSQLTable(OdbcConnection p_Connection, String p_Name)
        {
            if (p_Connection == null)
            {
                //throw new Spx_ORMMessage("SQLTable.createSQLTable(): the ODBC connection is null.",1); //@MdN 16/04/2015 - @MdN 15/08/2015 Livorno: cambiato il codice da 3 a 1
                throw new Spx_ORMMessage(_ERR_0, 1); //@MdN 01/10/2017 Monterotondo: introdotta variabile statica errore 0 - @MdN 16/04/2015 - @MdN 15/08/2015 Livorno: cambiato il codice da 3 a 1
            }

            // Livorno 15/08/2015 - Monterotondo 01/10/2017
            if (p_Name == null)
            {
                //throw new Spx_ORMMessage("SQLTable.createSQLTable(): table name not specified (is null).", 1);
                throw new Spx_ORMMessage(_ERR_00, 1); //@MdN 01/10/2017 Monterotondo: introdotta variabile statica errore 0
            }
            // Livorno 15/08/2015 - Monterotondo 01/10/2017

            SQLTable toRet = new SQLTable(p_Connection,p_Name);
            try
            {
                if (p_Connection.State == ConnectionState.Closed)                                           //@MdN 16/04/2015 - @MdN: 26/01/2020
                    p_Connection.Open();                                                                    //@MdN 16/04/2015 - @MdN: 26/01/2020
                toRet.loadInfo();
            }
            catch (Spx_ORMMessage EE)
            {
                EE.store();
                EE.Message = _ERR;
                EE.MessageCode = 0;
                throw EE;
            }
            finally
            {
                if (toRet.CurrentConnection.State == ConnectionState.Open)              //<added @MdN: 26/01/2020 />
                    toRet.CurrentConnection.Close();                                    //<added @MdN: 26/01/2020 />
                toRet.CurrentConnection.Close();    //<added @MdN: 26/01/2020 />
            }

            #region MEV SQLTable
            //ALTRE EVENTUALI FUNZIONI VANNO INSERITE DI SEGUITO.

            // fine
            #endregion
            p_Connection.Close();                                                                       //@MdN 16/04/2015
            return toRet;
        }

        /// <summary>
        /// <p>
        /// IT: Restituisce una collezione di stringhe (StringCollection) che rappresentano i nomi delle colonne
        /// della tabella che fanno parte della chiave primaria.
        /// </p>
        /// <p>
        /// EN: Get a System.Collections.Specialized.StringCollection object containing the names of the columns
        /// of the table that belong to the primary key.
        /// </p>
        /// <see cref="System.Collections.Specialized.StringCollection"/>
        /// <pre>
        /// ----
        /// @MdN
        /// Crtd: 21/09/2015
        /// </pre>
        /// </summary>
        public System.Collections.Specialized.StringCollection PrimaryKeyColumns
        {
            get
            {
                System.Collections.Specialized.StringCollection my_Collection = new System.Collections.Specialized.StringCollection();
                foreach (SQLColumn my_Columns in l_Columns)
                {
                    if(my_Columns.l_IsPrimayKey==true || my_Columns.l_IsKeyPart==true)
                        my_Collection.Add(my_Columns.l_Name);
                }
                return my_Collection;
            }
        }

        /// <summary>
        /// <p>
        /// IT: Restituisce una collezione di stringhe (StringCollection) che rappresentano i nomi delle colonne
        /// della tabella.
        /// </p>
        /// <p>
        /// EN: Get a System.Collections.Specialized.StringCollection object containing the names of the columns
        /// of the table.
        /// </p>
        /// <see cref="System.Collections.Specialized.StringCollection"/>
        /// <pre>
        /// ----
        /// @MdN
        /// Crtd: 23/10/2014
        /// </pre>
        /// </summary>
        public System.Collections.Specialized.StringCollection SQLColumns
        {
            get
            {
                System.Collections.Specialized.StringCollection my_Collection = new System.Collections.Specialized.StringCollection();
                foreach (SQLColumn my_Columns in l_Columns)
                    my_Collection.Add(my_Columns.l_Name);
                return my_Collection;
            }
        }

        /// <summary>
        /// <p>Carica un record.</p>
        /// Precondizione: valorizzazione della chiave.
        /// <pre>
        /// ----
        /// @MdN 
        /// Crtd: 19/05/2016
        /// </pre>
        /// </summary>
        public void load()
        {
            load(null);
        }

        /// <summary>
        /// <p>Carica un record.</p>
        /// Precondizione: valorizzazione della chiave.
        /// <pre>
        /// @MdN 
        /// Crtd: 08/01/2014
        /// --------------------
        /// Mdfd: @MdN 20/11/2014 Impostazione del flag l_changed.
        /// Mdfd: @MdN 19/05/2016 Introduzione della transazione.
        /// Mdfd: @MdN 01/10/2017 Correzione del testo errato di un messaggio di errore.
        /// Mdfd: @MdN 29/01/2020 (Fix #6 v .1.1.0.11) Tra le chiavi della tabella possono esserci anche colonne DateTime. Prima di questa modifica non era possibile effettuare il load di un oggetto dal database.
        /// </pre>
        /// </summary>
        /// <param name="p_trans">Transazione attiva: viene creata e gestita dal chiamante. Può essere null.</param>
        public void load(System.Data.Odbc.OdbcTransaction p_trans)
        {
            StringBuilder MySB = new StringBuilder();
            String MySql = null;
            SQLColumn MyTmpCol=null;
            Spx_ORMMessage MyMsg = null;
            OdbcCommand MyCommand = null;
            OdbcDataReader MyDR = null;
            System.Data.Odbc.OdbcTransaction _trans = p_trans;

            //verifica che la struttura informativa della tabella sia stata caricata 
            //in caso negativo --> CARICARLA.
            if (l_Columns == null)
                try
                {
                    this.loadInfo();
                }
                catch (Spx_ORMMessage E)
                {
                    //_ERR_1
                    // throw new Spx_ORMMessage("SQLTable.loadInfo(): Errors on loading table's informations: see inner messages for details.", 0, E);
                    throw new Spx_ORMMessage(_ERR_1, 0, E);

                }

            //verifica della specificazione della chiave
            //in caso negativo --> ECCEZIONE n.9. 
            if (this.hasPrimaryFullySpecified() == false)
                throw new Spx_ORMMessage(_ERR_2, 9);
                //throw new Spx_ORMMessage("SQLTable.load(): Primary Key not fully specified.", 9);

            MyMsg = new Spx_ORMMessage();

            //Caricamento
            // 1)-> composizione della query
            MySB.Append("SELECT * FROM ").Append(l_Name).Append(" WHERE ");
            //Ricerca della prima componente della chiave
            for (int t = 0; t < l_Columns.Count; t++)
            {
                MyTmpCol = (SQLColumn)l_Columns[t];
                if (MyTmpCol == null)
                {
                    MyMsg.store();
                    MyMsg.MessageCode = 0;
                    //MyMsg.Message = "SQLTable.load(): abnormal column list. Exit.";
                    MyMsg.Message = _ERR_3;
                    throw MyMsg;
                }
                if (MyTmpCol.l_IsPrimayKey == true)
                {
                    //trovata la prima (ed eventualmente unica) componente della chiave
                    while (MyTmpCol != null)
                    {
                        MySB.Append(MyTmpCol.l_Name).Append("=");
                        switch (MyTmpCol.SQLType)
                        {
                            case (int)Column.Column_Types.VARCHAR:
                            case (int)Column.Column_Types.TIMESTAMP:
                            //case (int)Column.Column_Types.TIME:         //<deleted @MdN: 29/01/2020 />
                            //case (int)Column.Column_Types.DATETIME:   //<deleted @MdN: 29/01/2020 />
                            //case (int)Column.Column_Types.DATE:         //<deleted @MdN: 29/01/2020 />
                            case (int)Column.Column_Types.CHAR:
                                {
                                    MySB.Append("'").Append(MyTmpCol.ColumnValue.Replace("'", "''")).Append("'");       
                                    break;
                                }
                            case (int)Column.Column_Types.DATETIME:                                                                             //<added @MdN: 29/01/2020 />
                            case (int)Column.Column_Types.TIME:                                                                                 //<added @MdN: 29/01/2020 />
                            case (int)Column.Column_Types.DATE:                                                                                 //<added @MdN: 29/01/2020 />
                                {
                                    MySB.Append("convert(DateTime, '").Append(MyTmpCol.ColumnValue.Replace("'", "''")).Append("', 102)");       //<added @MdN: 29/01/2020 />
                                    break;                                                                                                      //<added @MdN: 29/01/2020 />
                                }
                            case (int)Column.Column_Types.DECIMAL:
                            case (int)Column.Column_Types.DOUBLE:
                            case (int)Column.Column_Types.FLOAT:
                            case (int)Column.Column_Types.NUMERIC:
                            case (int)Column.Column_Types.REAL:
                                {
                                    MySB.Append(MyTmpCol.ColumnValue.Replace(",", "."));
                                    break;
                                }
                            default:
                                {
                                    MySB.Append(MyTmpCol.ColumnValue.Replace("'", "''"));
                                    break;
                                }
                        }
                        if (MyTmpCol.l_LastKeyPart != null)
                            MySB.Append(" AND ");
                        //continua
                        MyTmpCol = (SQLColumn) MyTmpCol.l_LastKeyPart;
                    } //Fine Ciclo
                    break;      //uscita dal ciclo for di ricerca
                }
            }//Fine Ciclo di ricerca
            
            //2-->Esecuzione della query
            MySql = MySB.ToString();
            //Console.Out.Write(MySql);       //DEBUG
            if (l_connection == null)
            {   //controllo connessione.
                MyMsg.store();
                //MyMsg.Message = "SQLTable.load(): Closed connection.";
                MyMsg.Message = _ERR_4;
                MyMsg.MessageCode = 3;
                throw MyMsg;
            }
            if (l_connection.State == ConnectionState.Closed)
                try
                {
                    _trans = null;          //@MdN 19/05/2016
                    l_connection.Open();
                }
                catch (Exception)
                {
                    MyMsg.store();
                    //MyMsg.Message = "SQLTable.load(): Unable to open the connection with the database.";
                    MyMsg.Message = _ERR_5;
                    MyMsg.MessageCode = 0;
                    throw MyMsg;                    
                }
            MyCommand = l_connection.CreateCommand();
            if (_trans != null)                         //@MdN 19/05/2016
                MyCommand.Transaction = _trans;         //@MdN 19/05/2016
            MyCommand.CommandText = MySql;
            try
            {
                MyDR=MyCommand.ExecuteReader();                
            }
            catch (Exception E)
            {
                MyMsg.store();
                MyMsg.InnerMessage = E;
                //MyMsg.Message = "SQLTable.load(): Error on the execution of the query '" + MySql + "'";
                MyMsg.Message = _ERR_6 + MySql + "'";
                MyMsg.MessageCode = 0;
                throw MyMsg;
            }
            if(MyDR.HasRows==true)
                MyDR.Read();
            else
            {
                MyMsg.store();
                //MyMsg.Message = "SQLTable.load(): The specificed record does not exist."; //<mdfd: @MdN 01/10/2017>
                MyMsg.Message = _ERR_7; //<mdfd: @MdN 01/10/2017>
                MyMsg.MessageCode = 8;
                throw MyMsg;
            }

            //3---> ciclo di lettura dei valori degli attributi
            for (int t = 0; t <= l_Columns.Count-1; t++)
            {
                MyTmpCol = (SQLColumn)l_Columns[t];
                Console.Out.Write(MyTmpCol.l_Name + " = ");       // DEBUG
                if (MyTmpCol.l_IsPrimayKey == true)
                    continue;   //salto la chiave perchè è già valorizzata
                //CASO PARTICOLARE DI DATE --> TRASFORMAZIONE DI FORMATO
                if (MyTmpCol.l_SQLType == (int)Column.Column_Types.DATE || MyTmpCol.l_SQLType == (int)Column.Column_Types.DATETIME || MyTmpCol.l_SQLType == (int)Column.Column_Types.TIME)
                    try
                    {
                        if(MyDR.IsDBNull(MyDR.GetOrdinal(MyTmpCol.l_Name))==false)
                            MyTmpCol.ColumnValue = Column.DateToSQLDateString(MyDR.GetDateTime(MyDR.GetOrdinal(MyTmpCol.l_Name)));
                    }
                    catch (Spx_ORMMessage ME)
                    {
                        MyMsg.store();
                        MyMsg.MessageCode = 0;
                        //MyMsg.Message = "SQLTable.load(): Error on parsing a date in SQL String format. See inner message for details";
                        MyMsg.Message = _ERR_8;
                        MyMsg.InnerMessage = ME;
                    }
                else
                {
                    //CASO ORDINARIO
                    try
                    {
                        Object TmpObj;
                        if (MyDR.IsDBNull(MyDR.GetOrdinal(MyTmpCol.l_Name)) == false)
                        {
                            TmpObj = MyDR[MyDR.GetOrdinal(MyTmpCol.l_Name)];
                            MyTmpCol.ColumnValue = TmpObj.ToString();
                        }
                            //MyTmpCol.ColumnValue = MyDR.GetString(MyDR.GetOrdinal(MyTmpCol.l_Name));
                    }
                    catch (Exception E)
                    {
                        MyMsg.store();
                        MyMsg.MessageCode = 0;
                        //MyMsg.Message = "SQLTable.load(): Error on . See inner message for details";
                        MyMsg.Message = _ERR_9;
                        MyMsg.InnerMessage = E;
                    }
                }
                Console.Out.WriteLine(MyTmpCol.l_Value);       // DEBUG
            } //fine ciclo di lettura dei valori degli attributi
            l_loaded = true;
            l_changed = false;                                  // @MdN 20/11/2014
            if (MyMsg.isNew == false)
                throw MyMsg;
            // FINE
        }


        /// <summary>
        /// Imposta il valore di un campo della tabella.
        /// <pre>
        /// @MdN 
        /// Crtd: 19/12/2013</pre>
        /// Mdfd: 20/11/2014: verifica che il vecchio valore ed il nuovo valore siano diversi; impostazione del campo l_changed in caso di aggiornamento.
        /// Mdfd: 08/12/2014: impostazione del flag l_modified
        /// Mdfd: 18/08/2015 - Livorno: se viene annullato il valore di una colonna chiave IDENTITY, l'oggetto diviene pronto per un inserimento.
        /// Mdfd: 25/01/2020 - v.1.1.0.10 
        ///                  - invocazione SQLColumn.isSQLDateString(p_ColValue);
        ///                  
        /// 
        /// </summary>
        /// <remarks>Lavora solo su stringhe! L'interpretazione dei valori delle stringhe è demandata alle procedure chiamanti.</remarks>
        /// <param name="p_ColName">Nome della colonna.</param>
        /// <param name="p_ColValue">Valora della colonna. E' consentito il valore null solo se la colonna è nullable.</param>
        public void setValue(String p_ColName, String p_ColValue)
        {
            SQLColumn tmpColumn = null;
            //ricerca
            for (int t = 0; t < l_Columns.Count; t++)
            {
                if (p_ColName.CompareTo(((SQLColumn)l_Columns[t]).l_Name) == 0)
                {
                    tmpColumn = (SQLColumn)l_Columns[t];

                    //@MdN 09/12/2014: la colonna ha un valore non null ma è una stringa vuota
                    //--> se la colonna non è di tipo VARCHAR diventa null.
                    if (p_ColValue != null && p_ColValue.Length == 0)
                    {
                        if (tmpColumn.SQLType != (int)Column.Column_Types.VARCHAR)
                            p_ColValue = null;
                    }

                    //@MdN 13/12/2014: CONTROLLO
                    if (p_ColValue != null)
                    {
                        switch (tmpColumn.l_SQLType)
                        {
                            case ((int)Column.Column_Types.DATE):
                            case ((int)Column.Column_Types.DATETIME):
                                {
                                    //if (Column.isSQLDateString(p_ColValue) == false) //<deleted @MdN: 25/01/2020 />
                                    if (SQLColumn.isSQLDateString(p_ColValue) == false) 
                                        //throw new simplex_ORM.Spx_ORMMessage("SQLTalble.setValue():Type mismatch error in column " + tmpColumn.Name + ": NOT A VALID DATE IN SQL-92 FORMAT.",12);
                                        throw new simplex_ORM.Spx_ORMMessage(_ERR_10.Replace("<%VALUE%>",tmpColumn.Name), 12);
                                    break;
                                }
                            case ((int)Column.Column_Types.INTEGER):
                            case ((int)Column.Column_Types.BIGINT):
                                {
                                    //Un intero è un numero NON decimale
                                    if (Column.isNumber(p_ColValue) == false || (Column.isNumber(p_ColValue) == true && Column.isDecimal(p_ColValue)==true))
                                        throw new simplex_ORM.Spx_ORMMessage(_ERR_11.Replace("<%VALUE%>", tmpColumn.Name), 12);
                                        //throw new simplex_ORM.Spx_ORMMessage("SQLTalble.setValue():Type mismatch error in column " + tmpColumn.Name + ": NOT A VALID NUMBER.", 12);
                                    break;
                                }
                            case ((int)Column.Column_Types.DECIMAL):
                            case ((int)Column.Column_Types.DOUBLE):
                            case ((int)Column.Column_Types.FLOAT):
                            case ((int)Column.Column_Types.NUMERIC):
                            case ((int)Column.Column_Types.REAL):
                                {
                                    //Un decimale è, comunque, un numero
                                    if (Column.isNumber(p_ColValue) == false)
                                        throw new simplex_ORM.Spx_ORMMessage("SQLTalble.setValue():Type mismatch error in column " + tmpColumn.Name + ": NOT A VALID DECIMAL NUMBER.", 12);
                                    break;
                                }
                            //<added @MdN 09/04/2020: issue #8 eseguire il troncamento automatico del valore SOLO in caso di valori stringa (VARCHAR, NVARCHAR, etc).>
                            case ((int)Column.Column_Types.VARCHAR):
                            case ((int)Column.Column_Types.CHAR):
                                {
                                    // eseguire il troncamento
                                    if (p_ColValue.Length > tmpColumn.l_NBytesColonna)
                                        p_ColValue = p_ColValue.Substring(0, tmpColumn.l_NBytesColonna);
                                    break;
                                }

                            default:
                                break;
                        }
                    }


                    //if (tmpColumn.l_IsNullable == false && p_ColValue == null)
                    if (tmpColumn.l_IsNullable == false && p_ColValue == null && tmpColumn.l_HasDefault==false && tmpColumn.l_IsPrimayKey==false) //@MdN 07/12/2014; @MdN 09/12/2014
                    {
                        Spx_ORMMessage EE = new Spx_ORMMessage("SQLTable.setValue(): Column " + p_ColName + " does not allow null value.", 6);
                        throw EE;
                    }
                    else
                    {
                        // Il ciclo salta se il valore non viene modificato
                        // VALORI DIVERSI --> MODIFICA
                        if ((p_ColValue != null && tmpColumn.ColumnValue != null) && (p_ColValue.CompareTo(tmpColumn.ColumnValue) != 0) || ((p_ColValue != null && tmpColumn.ColumnValue == null)))
                        {
                            tmpColumn.ColumnValue = p_ColValue;
                            tmpColumn.l_IsModified = true;                  //@MdN 08/12/2014
                            base.l_changed = l_changed | true;
                            return;
                        }

                        // IL VALORE CORRENTE NON NULLO VIENE AGGIORNATO DA UN VALORE NULL
                        if (p_ColValue == null && tmpColumn.ColumnValue != null)
                        {   
                         
                            /* @MdN - 18/08/2015 - Livorno
                             * vi è un sottocaso: se la colonna che ha valore non nullo è chiave primaria di tipo 
                             * IDENTITY (attenzione, deve essere chiave primaria, NON essere parte di una chiave
                             * primaria), i flag l_changed e l_loaded tornano a false in modo che un eventuale 
                             * successiva operazione di salvataggio si traduca in un INSERT. 
                             * */
                            tmpColumn.ColumnValue = p_ColValue;
                            tmpColumn.l_IsModified = true;                  //@MdN 08/12/2014

                            if (tmpColumn.l_IsIdentity == true && tmpColumn.l_IsPrimayKey == true && tmpColumn.l_KeyCount == 1) // @MdN - 18/08/2015 - Livorno
                            {                                                                                                   // @MdN - 18/08/2015 - Livorno
                                base.l_changed = false;                                                                         // @MdN - 18/08/2015 - Livorno
                                base.l_loaded = false;                                                                          // @MdN - 18/08/2015 - Livorno
                            }                                                                                                   // @MdN - 18/08/2015 - Livorno
                            else
                                base.l_changed = l_changed | true;
                            return;
                        }

                        // IL VALORE CORRENTE NULLO VIENE AGGIORNATO DA UN VALORE NON NULLO //@MdN 13/12/2014
                        // DA FATTORIZZARE !!!!
                        if (p_ColValue != null && tmpColumn.ColumnValue != null)
                        {
                            tmpColumn.ColumnValue = p_ColValue;
                            tmpColumn.l_IsModified = true;                  //@MdN 08/12/2014
                            base.l_changed = l_changed | true;
                            return;
                        }

                        // CASO RESIDUALE: UN VALORE NULL VIENE AGGIORNATO DA UN VALORE NULL --> NULLA DI FATTO
                        return;    
                    }
                }
            }// fine ciclo.
            throw new Spx_ORMMessage("SQLTable.setValue(): Column " + p_ColName + " does not exist.", 7);            
        }

        /// <summary>
        /// Restituisce, in forma di stringa, il valore della colonna specificata.
        /// <pre>
        /// ----
        /// Crtd: 19/12/2013
        /// Mdfd: 22/07/2018
        /// @MdN
        /// </pre>
        /// </summary>
        /// <param name="p_ColName">Nome della colonna di cui si desidera ottenere il valore.</param>
        /// <returns>Valore della colonna specifcata.</returns>
        public String getValue(String p_ColName)
        {
            // SQLColumn tmpColumn = null;      //<deleted @MdN: 25/01/2020 />

            //ricerca
            for (int t = 0; t < l_Columns.Count; t++)
            {
                if (p_ColName.CompareTo(((SQLColumn)l_Columns[t]).l_Name) == 0)
                {
                    return ((SQLColumn)l_Columns[t]).ColumnValue;
                }
            }// fine ciclo.
            // Spx_ORMMessage EE = new Spx_ORMMessage("SQLTable.getValue(): Column " + p_ColName + " don't exists.", 7); //<deleted @MdN: 22/07/2018>
            Spx_ORMMessage EE = new Spx_ORMMessage("SQLTable.getValue(): Table " + Name + ", Column " + p_ColName + " does not exist.", 7); //<added @MdN: 22/07/2018>
            throw EE;
        }

        /// <summary>
        /// <p>
        /// Crea e restituisce un oggetto di classe SQLForeignKey che contiene tutte le informazioni
        /// sull'eventuale vincoli di integrità referenziale a chiave esterna che insiste su una data
        /// colonna della tabella. Se sulla colonna specificata non sussistono vincoli di chiave esterna
        /// il metodo restituisce null.
        /// </p>
        /// <pre>
        /// ----
        /// @MdN
        /// Crtd: 02/05/2015
        /// </pre>
        /// </summary>
        /// <param name="p_ColumnName">nome della colonna di indagine</param>
        /// <returns>un oggetto di classe SQLForeignKey se il vincolo esiste, null altrimenti.</returns>
        public simplex_ORM.SQLSrv.SQLForeignKey getForeignKey(String p_ColumnName)
        {
            simplex_ORM.SQLSrv.SQLForeignKey _toRet = null;
            
            //controllo
            if (p_ColumnName == null)
                return null;

            //creazione
            _toRet = new SQLForeignKey(this);
            if (_toRet == null)
                return null;

            //caricamento informazioni
            if (_toRet.load(p_ColumnName) == false)
                return null;
            else
                return _toRet;
        }

        /// <summary>
        /// Stampa informazioni sulla tabella.
        /// </summary>
        /// <returns>Stringa che mostra campi e tabella.</returns>
        public override String ToString()
        {
            StringBuilder toREt = new StringBuilder();
            toREt.AppendLine(this.l_Name);
            foreach(SQLColumn TmpCol in this.l_Columns)
                toREt.Append(TmpCol.l_Name).Append("(").Append(TmpCol.l_SQLType).Append(")").Append(" = ").AppendLine(TmpCol.l_Value);
            return toREt.ToString();
        }

        /// <summary>
        /// <p>IT: Salva il contenuto dell'oggetto nella corrispondente tabella.</p>
        /// <p>EN: Saves the object's content in the related table.</p>
        /// <pre>
        /// ----
        /// @MdN
        /// Crtd: 20/11/2014
        /// Mdfd: @MdN 19/05/2016 Introduzione della transazione.
        /// --------------------
        /// </pre>
        /// </summary>
        /// <param name="p_nocheck">
        /// <p>IT: Flag che indica se effettuare alcuni controlli di cosistenza: normalmente impostare a true</p>.
        /// <p>EN: Flag that states if the procedure has to execute some controls: set normally to the 'true' value</p>.
        /// </param>
        /// <param name="p_trans">Transazione attiva: viene creata e gestita dal chiamante. Può essere null.</param>
        /// <see>SQLTable.insert(bool)</see>
        public void save(bool p_nocheck, System.Data.Odbc.OdbcTransaction p_trans)
        {
            System.Data.Odbc.OdbcTransaction _trans = p_trans;

            try
            {
                if (l_loaded == false)
                    insert(p_nocheck, _trans);
                else
                    if (l_changed == true)
                        update(_trans);
            }
            catch (Spx_ORMMessage E)
            {
                if (E.MessageCode == 10)
                {
                    if (l_changed == true)
                        update(_trans);
                }
                else
                    throw E;    //BUBBLEING OF ERROR // RISALITA DELL'ERRORE.
            }
            l_changed = false;
            l_loaded = true;
        }

        /// <summary>
        /// <p>IT: Salva il contenuto dell'oggetto nella corrispondente tabella.</p>
        /// <p>EN: Saves the object's content in the related table.</p>
        /// <pre>
        /// @MdN
        /// Crtd: 20/11/2014
        /// --------------------
        /// </pre>
        /// </summary>
        /// <param name="p_nocheck">
        /// <p>IT: Flag che indica se effettuare alcuni controlli di cosistenza: normalmente impostare a true</p>.
        /// <p>EN: Flag that states if the procedure has to execute some controls: set normally to the 'true' value</p>.
        /// </param>
        /// <see>SQLTable.insert(bool)</see>
        public void save(bool p_nocheck)
        {
            try
            {
                if (l_loaded == false)
                    insert(p_nocheck);
                else
                    if(l_changed==true)
                        update();
            }
            catch (Spx_ORMMessage E)
            {
                if (E.MessageCode == 10)
                {
                    if (l_changed == true)
                        update();
                }
                else
                    throw E;    //BUBBLEING OF ERROR // RISALITA DELL'ERRORE.
            }
            l_changed = false;
            l_loaded = true;
        }

        /// <summary>
        /// Verifica se la colonna il cui nome è passato per argomento è stata modificata dall'ultimo caricamento.<br></br>
        /// 
        /// <pre>
        /// ----
        /// Crtd: 10/04/2020 v.1.1.0.12 - issue #9 - SQLTable(). Esporre un metodo pubblico per verificare se un dato attributo ha subito una modifica.
        /// </pre>
        /// </summary>
        /// <param name="p_ColumnName">Nome della colonna da verificare.</param>
        /// <returns>True se è stata modificata, false in ogni altro caso, errori compresi.</returns>
        public Boolean isModified(String p_ColumnName)
        {
            foreach(Column _col in this.l_Columns)
            {
                if (_col.Name.CompareTo(p_ColumnName) == 0)
                {
                    return _col.l_IsModified;
                }
            }
            return false;
        }

        /// <summary>
        /// <p>
        /// Verifica se il tipo di dato della colonna il cui nome viene passato
        /// come parametro è di tipo testuale (CHAR, VARCHAR, NVARCHAR ETC...).
        /// <pre>
        /// ----
        /// @MdN
        /// Crtd: 03/05/2015
        /// </pre>
        /// </p>
        /// </summary>
        /// <param name="p_columnName">Nome della colonna.</param>
        /// <returns><b>True</b> se la colonna appartiene ad un dominio testuale, <b>False</b> altrimenti.</returns>
        /// <remarks>
        /// Nel caso la colonna specificata non appartenesse alla tabella, viene lanciata
        /// un'eccezione di tipo <see cref="simplex_ORM.Spx_ORMMessage"/> con codice 7.
        /// </remarks>
        public Boolean isTextField(String p_columnName)
        {
            // Boolean _toRet = false;          //<deleted @MdN: 25/01/2020 />
            // SQLColumn _tmpcol = null;        //<deleted @MdN: 25/01/2020 />
            
            //controllo
            if (p_columnName == null)
                return false;

            foreach (SQLColumn _c in l_Columns)
            {
                if (_c.Name.CompareTo(p_columnName) == 0)
                {
                    // TROVATO!!!
                    if ((_c.SQLType == (int)Column.Column_Types.CHAR) ||
                        (_c.SQLType == (int)Column.Column_Types.VARCHAR))
                        return true;
                    else
                        return false;
                }
            }
            throw new simplex_ORM.Spx_ORMMessage(7,p_columnName,"SQLTable.isTextField(): La colonna " + p_columnName + " non appartiene alla tabella " + l_Name + ".");
        }

        /// <summary>
        /// <p>
        /// Verifica se il tipo di dato della colonna il cui nome viene passato
        /// come parametro è di tipo bit/booleano (BIT).
        /// <pre>
        /// ----
        /// @MdN
        /// Crtd: 03/05/2015
        /// </pre>
        /// </p>
        /// </summary>
        /// <param name="p_columnName">Nome della colonna.</param>
        /// <returns><b>True</b> se la colonna appartiene ad un dominio booleano, <b>False</b> altrimenti.</returns>
        /// <remarks>
        /// Nel caso la colonna specificata non appartenesse alla tabella, viene lanciata
        /// un'eccezione di tipo <see cref="simplex_ORM.Spx_ORMMessage"/> con codice 7.
        /// </remarks>
        public Boolean isBitField(String p_columnName)
        {
            // Boolean _toRet = false;      //<deleted @MdN: 25/01/2020 />
            // SQLColumn _tmpcol = null;    //<deleted @MdN: 25/01/2020 />

            //controllo
            if (p_columnName == null)
                return false;
            foreach (SQLColumn _c in l_Columns)
            {
                if (_c.Name.CompareTo(p_columnName) == 0)
                {
                    // TROVATO!!!
                    if (_c.SQLType == (int)Column.Column_Types.BIT)
                        return true;
                    else
                        return false;
                }
            }
            throw new simplex_ORM.Spx_ORMMessage(7, p_columnName, "SQLTable.isBitField():La colonna " + p_columnName + " non appartiene alla tabella " + l_Name + ".");
        }

        /// <summary>
        /// <p>
        /// Verifica se il tipo di dato della colonna il cui nome viene passato
        /// come parametro è di numerico con virgola (DECIMAL, FLOAT, DOUBLE, REAL, ETC...).
        /// <pre>
        /// ----
        /// @MdN
        /// Crtd: 01/10/2017 - Monterotondo
        /// </pre>
        /// </p>
        /// </summary>
        /// <param name="p_columnName">Nome della colonna.</param>
        /// <returns><b>True</b> se la colonna appartiene ad un dominio testuale, <b>False</b> altrimenti.</returns>
        /// <remarks>
        /// Nel caso la colonna specificata non appartenesse alla tabella, viene lanciata
        /// un'eccezione di tipo <see cref="simplex_ORM.Spx_ORMMessage"/> con codice 7.
        /// </remarks>
        public Boolean isDecimalField(String p_columnName)
        {
            // Boolean _toRet = false;          //<deleted @MdN: 25/01/2020 />
            // SQLColumn _tmpcol = null;        //<deleted @MdN: 25/01/2020 />

            //controllo
            if (p_columnName == null)
                return false;

            foreach (SQLColumn _c in l_Columns)
            {
                if (_c.Name.CompareTo(p_columnName) == 0)
                {
                    // TROVATO!!!
                    if ((_c.SQLType == (int)Column.Column_Types.DOUBLE) ||
                        (_c.SQLType == (int)Column.Column_Types.FLOAT)  ||
                        (_c.SQLType == (int)Column.Column_Types.DECIMAL)||
                        (_c.SQLType == (int)Column.Column_Types.REAL))                        
                        return true;
                    else
                        return false;
                }
            }
            throw new simplex_ORM.Spx_ORMMessage(7, p_columnName, "SQLTable.isDecimalField(): La colonna " + p_columnName + " non appartiene alla tabella " + l_Name + ".");
        }

        /// <summary>
        /// <p>
        /// Verifica se il tipo di dato della colonna il cui nome viene passato
        /// come parametro è di numerico intero (BIGINT, SMALLINT, INT ETC...).
        /// <pre>
        /// ----
        /// @MdN
        /// Crtd: 01/10/2017 - Monterotondo
        /// </pre>
        /// </p>
        /// </summary>
        /// <param name="p_columnName">Nome della colonna.</param>
        /// <returns><b>True</b> se la colonna appartiene ad un dominio testuale, <b>False</b> altrimenti.</returns>
        /// <remarks>
        /// Nel caso la colonna specificata non appartenesse alla tabella, viene lanciata
        /// un'eccezione di tipo <see cref="simplex_ORM.Spx_ORMMessage"/> con codice 7.
        /// </remarks>
        public Boolean isIntegerField(String p_columnName)
        {
            // Boolean _toRet = false;          //<deleted @MdN: 25/01/2020 />
            // SQLColumn _tmpcol = null;        //<deleted @MdN: 25/01/2020 />

            //controllo
            if (p_columnName == null)
                return false;

            foreach (SQLColumn _c in l_Columns)
            {
                if (_c.Name.CompareTo(p_columnName) == 0)
                {
                    // TROVATO!!!
                    if ((_c.SQLType == (int)Column.Column_Types.INTEGER) ||
                        (_c.SQLType == (int)Column.Column_Types.BIGINT)  ||
                        (_c.SQLType == (int)Column.Column_Types.SMALLINT))
                        return true;
                    else
                        return false;
                }
            }
            throw new simplex_ORM.Spx_ORMMessage(7, p_columnName, "SQLTable.isIntegerField(): La colonna " + p_columnName + " non appartiene alla tabella " + l_Name + ".");
        }

        /// <summary>
        /// <p>
        /// Verifica se il tipo di dato della colonna il cui nome viene passato
        /// come parametro è di numerico DateTime (DATE, TIME, DATETIME ETC...).
        /// <pre>
        /// ----
        /// @MdN
        /// Crtd: 01/10/2017 - Monterotondo
        /// </pre>
        /// </p>
        /// </summary>
        /// <param name="p_columnName">Nome della colonna.</param>
        /// <returns><b>True</b> se la colonna appartiene ad un dominio testuale, <b>False</b> altrimenti.</returns>
        /// <remarks>
        /// Nel caso la colonna specificata non appartenesse alla tabella, viene lanciata
        /// un'eccezione di tipo <see cref="simplex_ORM.Spx_ORMMessage"/> con codice 7.
        /// </remarks>
        public Boolean isDateTimeField(String p_columnName)
        {
            // Boolean _toRet = false;          //<deleted @MdN: 25/01/2020 />
            // SQLColumn _tmpcol = null;        //<deleted @MdN: 25/01/2020 />

            //controllo
            if (p_columnName == null)
                return false;

            foreach (SQLColumn _c in l_Columns)
            {
                if (_c.Name.CompareTo(p_columnName) == 0)
                {
                    // TROVATO!!!
                    if ((_c.SQLType == (int)Column.Column_Types.DATE) ||
                        (_c.SQLType == (int)Column.Column_Types.DATETIME) ||
                        (_c.SQLType == (int)Column.Column_Types.TIME))
                        return true;
                    else
                        return false;
                }
            }
            throw new simplex_ORM.Spx_ORMMessage(7, p_columnName, "SQLTable.isIntegerField(): La colonna " + p_columnName + " non appartiene alla tabella " + l_Name + ".");
        }


        /// <summary>
        /// <p>
        /// Creazione di un oggetto LookupBrowser, previ controlli.
        /// Nella versione attuale, l'oggetto creato restituisce un valore di tipo stringa
        /// specificando il corrispondente valore dela chiave PRIMARIA.
        /// </p>
        /// <pre>
        /// ----
        /// @MdN
        /// Crtd: 08/08/2015
        /// </pre>
        /// </summary>
        /// <param name="p_Key">Nome della colonna della tabella che referenzia la chiave della tabella di lookup.</param>
        /// <param name="p_Value">Nome della colonna della tabella di lookup che contiene il valore della chiave specficata.</param>
        /// <returns></returns>
        public SQLLookupBrowser getLookupBrowser(String p_Key, String p_Value)
        {
            // controlli
            simplex_ORM.SQLSrv.SQLColumn _valCol = null;
            simplex_ORM.SQLSrv.SQLColumn _keyCol = null;
            int _BrowserType = 0;

            // 1) i parametri p_Key e p_Value sono realmente colonne della tabella?
            foreach(simplex_ORM.SQLSrv.SQLColumn _col in this.l_Columns)
            {
                /*
                 * ATTENZIONE!!!
                 * E' stata scoperta un'anomalia nella query principale del metodo loadInfo() allorché
                 * una colonna partecipi in più indici: in questi casi la colonna viene estratta dalla
                 * query tante volte quanti sono gli indici a cui partecipa. Tale anomalia dovrà essere
                 * corretta in futuro perché può essre causa di potenziali errori.
                 * 
                 * E' stato qui applicato un WORKAROUND: si considera solo la prima occorrenza della colonna
                 * a meno che la seconda occorrenza sia associata ad vincolo di chiave primaria.
                 */
                if (_col.Name.ToLower().CompareTo(p_Key.ToLower()) == 0)
                {
                    if (_keyCol == null)
                        _keyCol = _col;
                    else
                    {
                        if (_col.l_IsPrimayKey == true)
                            _keyCol = _col;
                    }
                }
                if (_col.Name.ToLower().CompareTo(p_Value.ToLower()) == 0)
                {
                    if (_valCol == null)
                        _valCol = _col;
                    else
                    {
                        if (_col.l_IsPrimayKey == true)
                            _valCol = _col;
                    }
                }
            }
            if (_valCol == null)
            {
                simplex_ORM.Spx_ORMMessage ormm = new Spx_ORMMessage("La tabella non contiene la colonna 'valore' specificata.", 7);
                throw ormm;
            }
            if (_keyCol == null)
            {
                simplex_ORM.Spx_ORMMessage ormm = new Spx_ORMMessage("La tabella non contiene la colonna 'chiave' specificata.", 7);
                throw ormm;
            }
            
            // NOTA: _valCole e _keyCol contengono le colonne specificate.

            // Verifica che l'argomento p_Key sia una chiave primaria.
            if (_keyCol.l_IsPrimayKey == false)
            {
                simplex_ORM.Spx_ORMMessage ormm = new Spx_ORMMessage("La colonna 'chiave' specificata non è una chiave primaria.", 7);
                throw ormm;
            }

            // Verifica che il tipo p_Key sia un valore intero o un valore stringa
            switch (_keyCol.l_SQLType)
            {
                case ((int)Column.Column_Types.CHAR):
                case ((int)Column.Column_Types.VARCHAR):
                    {
                        _BrowserType = 1;
                        break;
                    }
                case ((int)Column.Column_Types.BIGINT):
                case ((int)Column.Column_Types.INTEGER):
                case ((int)Column.Column_Types.SMALLINT):
                    {
                        _BrowserType = 0;
                        break;
                    }
                default:
                    {
                        simplex_ORM.Spx_ORMMessage ormm = new Spx_ORMMessage("La colonna 'chiave' specificata non è di un tipo attualmente supportato.", 12);
                        throw ormm;
                    }
            }

            // Ora è possibile creare e restituire l'oggetto
            SQLLookupBrowser _toRet = new SQLLookupBrowser(p_Key, p_Value, this.l_Name, this.CurrentConnection);
            _toRet.KeyType = _BrowserType;
            return _toRet;
        }

        #endregion

        #region PROPERTIES PUBBLICHE
        /// <summary>
        /// Verifica se un oggetto ORM è stato caricato dal database (true) o se è nuovo (false).<br></br>
        /// <remarks>
        /// Quando un oggetto ORM nuovo viene salvato nel database mediante i metodi save() e/o insert(), la condizione 
        /// "Loaded" passa da false a true.<br></br>
        /// </remarks>
        /// <pre>
        /// ----
        /// @MdN
        /// Crtd: 01/01/2017 - Monterotondo - v.1.0.0.0
        /// </pre>
        /// </summary>
        public Boolean Loaded
        {
            get
            {
                return l_loaded;
            }
        }

        /// <summary>
        /// <p>
        /// IT: restituisce la connessione attiva corrente.
        /// </p>
        /// <p>
        /// EN: Gets the current active connection.
        /// </p>
        /// <pre>
        /// ----
        /// @MdN: 
        /// Crtd: 15/11/2014
        /// RO
        /// </pre>
        /// </summary>
        public System.Data.Odbc.OdbcConnection CurrentConnection
        {
            get
            {
                return this.l_connection;
            }
        }
        #endregion

        #region METODI PRIVATI E PROTETTI

        /// <summary>
        /// Costruttore privato.
        /// @MdN - 12/12/2013
        /// </summary>
        /// <param name="p_Connection">Connessione.</param>
        private SQLTable(OdbcConnection p_Connection)
        {
            l_connection = p_Connection;
            l_Columns = new System.Collections.ArrayList();
            l_loaded = false;
            l_changed = false;
        }


        /// <summary>
        /// Costruttore privato.
        /// @MdN - 12/12/2013
        /// </summary>
        /// <param name="p_Connection">Connessione.</param>
        /// <param name="p_Name">Nome della tabella da creare e caricare.</param>
        private SQLTable(OdbcConnection p_Connection, String p_Name)
            : this(p_Connection)
        {
            //l_connection = p_Connection;
            l_Name = p_Name;
        }


        /// <summary>
        /// Verifica che siano valorizzati:
        ///  - tutti gli attributi chiave e NON identity 
        ///  - tutti gli attributi NOT NULL che NON hanno un valore di default.
        ///  <pre>
        ///  ----
        ///  @MdN
        ///  Crtd: 21/01/2014
        ///  </pre>
        /// </summary>
        /// <returns> Può restituire solo un valore
        /// <ul>
        /// <li>TRUE: il record può essere inserito;</li>
        /// <li>ECCEZIONE: il record NON può essere inserito e perchè.</li>
        /// </ul>
        /// </returns>        
        private Boolean verifyForInsert()
        {
            Boolean toRet = true;
            if (l_Columns == null)
            {
                throw new Spx_ORMMessage("SQLTable.verifyForInsert(): nothing to verify.", 8);
            }
            SQLColumn MyCol = null;
            for (int t = 0; t < l_Columns.Count; t++)
            {
                MyCol = (SQLColumn)l_Columns[t];
                if (MyCol.l_IsNullable == false)
                    if (MyCol.l_HasDefault == false && MyCol.l_IsIdentity == false)  //Se la colonna NON può essere nullo e NON ha valori di default e non è identity --> CONTROLLA; @MdN 08/12/2014 - cambiato (||) in (&&)
                        if (MyCol.l_Value == null)
                        {
                            //@MdN 08/12/2014: il metodo non restituisce più FALSE ma lancia un'eccezione di tipo 6 indicando la colonna che ha originato la violazione!
                            throw new Spx_ORMMessage("SQLTable.verifyForInsert(): column " + MyCol.Name + " does not allow a null value and does not have a default value.", 6);
                        }
            }
            return toRet;
        }

        /// <summary>
        /// Carica le informazioni sulla tabella.
        /// ----
        /// @MdN 
        /// Crtd: 09/05/2016
        /// </summary>
        /// <exception cref="Spx_ORMMessage"></exception>
        private void loadInfo()
        {
            loadInfo(null);
        }

        /// <summary>
        /// Carica le informazioni sulla tabella.
        /// ----
        /// @MdN 
        /// Crtd: 12/12/2013
        /// Mdfd: @MdN 08/12/2014;
        /// Mdfd: @MdN 09/05/2016 - Introduzione transazioni;
        /// </summary>
        /// <param name="p_trans">Transazione attiva: viene creata e gestita dal chiamante. Può essere null.</param>
        /// <exception cref="Spx_ORMMessage"></exception>
        private void loadInfo(System.Data.Odbc.OdbcTransaction p_trans)
        {
            String MySql = null;
            OdbcCommand MyCommand = null;
            OdbcDataReader MyDataR = null;
            SQLColumn MyColumn = null;
            SQLColumn MyLostKeyPart = null;
            String MyName;
            String MyTabName = null;
            String MySchemaName = null;
            String[] MyTabNames = null;
            char[] MySeparators = new char[1];
            int MyType;

            System.Data.Odbc.OdbcTransaction _trans = null;             //@MdN 19/05/2016
            _trans = p_trans;                                           //@MdN 19/05/2016

            Spx_ORMMessage EE = null;

            if (l_connection == null)
            {
                throw new Spx_ORMMessage("SQLTable.loadInfo(): Closed connection.", 3);
            }

            // separazione tra nome tabella e schema
            MySeparators[0] = '.';
            MyTabNames = l_Name.Split(MySeparators);
            if (MyTabNames.Length > 1)
            {
                MySchemaName = MyTabNames[0];
                MyTabName = MyTabNames[1];
            }
            else
            {
                MySchemaName = "dbo";
                MyTabName = l_Name;
            }

            /* * COMPOSIZIONE DELLA QUERY * */
            //---> test OK!!!!!
            /* 
             * DELETED @MdN: 19/05/2017
            MySql = "select SO1.ID";
            MySql = MySql + ", SO1.name as [NomeTabella]";
            MySql = MySql + ", SO1.xtype as [TipoTabella]";
            MySql = MySql + ", SCL.name as [NomeColonna]";
            MySql = MySql + ", SCL.xtype as [TipoColonna]";
            MySql = MySql + ", SCL.length as [ByteColonna]";
            MySql = MySql + ", SCL.prec as [PrecisioneColonna]";
            MySql = MySql + ", SCL.scale as [ScalaColonna]";
            MySql = MySql + ", SCL.colid as [IdColonna]";
            MySql = MySql + ", cast(SCL.isnullable as bit) as isNullable";
            MySql = MySql + ", SCL.iscomputed";
            MySql = MySql + ", SI.name as [NomeIndice]";
            MySql = MySql + ", SI.status as [StatoIndice]";
            MySql = MySql + ", SI.keycnt as [NumeroChiavi]";
            MySql = MySql + ", CASE WHEN ((SI.status & 2048)=2048) THEN CAST(1 AS BIT) ELSE CAST(0 AS BIT) END as [isPrimary]";
            MySql = MySql + ", CASE WHEN ((SI.status & 2)=2) THEN CAST(1 AS BIT) ELSE CAST(0 AS BIT) END as [isUnique]";
            MySql = MySql + ", SIK.indid as [IDIndice]";
            MySql = MySql + ", SIK.colid";
            MySql = MySql + ", SKS.isForeignKey";
            MySql = MySql + ", CASE WHEN (ISNULL(SCL.cdefault,0)>0) THEN 1 ELSE 0 END AS [hasDefault]";
            MySql = MySql + ", CASE WHEN ((SCL.status & 128)=128) THEN CAST(1 AS BIT) ELSE CAST(0 AS BIT) END AS [isIdentity]";
            MySql = MySql + " from sysobjects SO1 INNER JOIN syscolumns SCL ON SCL.id=SO1.id";
            MySql = MySql + " LEFT JOIN sysindexkeys SIK ON SIK.ID=SO1.ID and SIK.colid=SCL.colid ";
            MySql = MySql + " LEFT JOIN sysindexes SI ON SI.id=SCL.id and SI.indid=SIK.indid ";
            MySql = MySql + " LEFT JOIN (";
                MySql = MySql + " SELECT SKK.id";
                MySql = MySql + ", SKK.colid";
                MySql = MySql + ", CAST(CASE WHEN SKK.HasDefaultValue >0 THEN 1 ELSE 0 END as bit) AS hasDefault";
                MySql = MySql + ", CAST(CASE WHEN SKK.IsUniqueConst >0 THEN 1 ELSE 0 END as bit) AS isUnique";
                MySql = MySql + ", CAST(CASE WHEN SKK.IsForeignKey >0 THEN 1 ELSE 0 END as bit) AS isForeignKey";
                MySql = MySql + ", CAST(CASE WHEN SKK.IsPrimaryKey >0 THEN 1 ELSE 0 END as bit) AS isPrimary";
                MySql = MySql + " FROM ";
                MySql = MySql + " (";
                MySql = MySql + " SELECT ";
                MySql = MySql + "SK.id";
                MySql = MySql + ", SK.colid";
                MySql = MySql + ", sum(CASE WHEN SK.status & 32 = 32 then 1 else 0 END) as TableLevelConst";
                MySql = MySql + ", sum(CASE WHEN SK.status & 16 = 16 then 1 else 0 END) as ColumnLevelConst";
                MySql = MySql + ", sum(CASE WHEN ((SK.status & 5 = 5) AND (SK.status & 16=16))	then 1 else 0 END) as HasDefaultValue";
                MySql = MySql + ", sum(CASE WHEN ((SK.status & 4 = 4) AND (SK.status & 16=16))	then 1 else 0 END) as IsCheckConst";
                MySql = MySql + ", sum(CASE WHEN ((SK.status & 3 = 3) AND (SK.status & 16=16))	then 1 else 0 END) as IsForeignKey";
                MySql = MySql + ", sum(CASE WHEN ((SK.status & 2 = 2) AND (SK.status & 32=32))	then 1 else 0 END) as IsUniqueConst";
                MySql = MySql + ", sum(CASE WHEN ((SK.status & 1 = 1) AND (SK.status & 32=32))	then 1 else 0 END) as IsPrimaryKey";
                MySql = MySql + " FROM SYSCONSTRAINTS SK";
                MySql = MySql + " GROUP BY";
                MySql = MySql + " SK.id";
                MySql = MySql + ", SK.colid";
                MySql = MySql + ") AS SKK ";
            MySql = MySql + ") AS SKS ON SKS.id=SIK.id AND SKS.colid=SIK.colid";
            MySql = MySql + " LEFT JOIN sysusers SU ON SO1.uid=SU.uid ";
            MySql = MySql + " WHERE SO1.xtype='U' AND SO1.name='" + MyTabName + "' AND SU.name='" + MySchemaName + "' ORDER BY SCL.colorder";
             * DELETED @MdN:19/05/2017
             * */

            //<added @MdN: 19/05/2017>
            MySql = "select DISTINCT SO1.ID";
            MySql = MySql + ", SCL.colorder";
            MySql = MySql + ", SO1.name as [NomeTabella]";
            MySql = MySql + ", SO1.xtype as [TipoTabella]";
            MySql = MySql + ", SCL.name as [NomeColonna]";
            MySql = MySql + ", SCL.xtype as [TipoColonna]";
            MySql = MySql + ", SCL.length as [ByteColonna]";
            MySql = MySql + ", SCL.prec as [PrecisioneColonna]";
            MySql = MySql + ", SCL.scale as [ScalaColonna]";
            MySql = MySql + ", SCL.colid as [IdColonna]";
            MySql = MySql + ", cast(SCL.isnullable as bit) as isNullable";
            MySql = MySql + ", SCL.iscomputed";
            //MySql = MySql + ", SI.name as [NomeIndice]";
            //MySql = MySql + ", SI.status as [StatoIndice]";
            MySql = MySql + ", SI.keycnt as [NumeroChiavi]";
            MySql = MySql + ", CASE WHEN ((SI.status & 2048)=2048) THEN CAST(1 AS BIT) ELSE CAST(0 AS BIT) END as [isPrimary]";
            MySql = MySql + ", CASE WHEN ((SI.status & 2)=2) THEN CAST(1 AS BIT) ELSE CAST(0 AS BIT) END as [isUnique]";
            //MySql = MySql + ", SIK.indid as [IDIndice]";
            MySql = MySql + ", SIK.colid";
            MySql = MySql + ", SKS.isForeignKey";
            MySql = MySql + ", CASE WHEN (ISNULL(SCL.cdefault,0)>0) THEN 1 ELSE 0 END AS [hasDefault]";
            MySql = MySql + ", CASE WHEN ((SCL.status & 128)=128) THEN CAST(1 AS BIT) ELSE CAST(0 AS BIT) END AS [isIdentity]";
            MySql = MySql + " from sysobjects SO1 INNER JOIN syscolumns SCL ON SCL.id=SO1.id";
            MySql = MySql + " LEFT JOIN sysindexkeys SIK ON SIK.ID=SO1.ID and SIK.colid=SCL.colid ";
            MySql = MySql + " LEFT JOIN sysindexes SI ON SI.id=SCL.id and SI.indid=SIK.indid ";
            MySql = MySql + " LEFT JOIN (";
            MySql = MySql + " SELECT SKK.id";
            MySql = MySql + ", SKK.colid";
            MySql = MySql + ", CAST(CASE WHEN SKK.HasDefaultValue >0 THEN 1 ELSE 0 END as bit) AS hasDefault";
            MySql = MySql + ", CAST(CASE WHEN SKK.IsUniqueConst >0 THEN 1 ELSE 0 END as bit) AS isUnique";
            MySql = MySql + ", CAST(CASE WHEN SKK.IsForeignKey >0 THEN 1 ELSE 0 END as bit) AS isForeignKey";
            MySql = MySql + ", CAST(CASE WHEN SKK.IsPrimaryKey >0 THEN 1 ELSE 0 END as bit) AS isPrimary";
            MySql = MySql + " FROM ";
            MySql = MySql + " (";
            MySql = MySql + " SELECT ";
            MySql = MySql + "SK.id";
            MySql = MySql + ", SK.colid";
            MySql = MySql + ", sum(CASE WHEN SK.status & 32 = 32 then 1 else 0 END) as TableLevelConst";
            MySql = MySql + ", sum(CASE WHEN SK.status & 16 = 16 then 1 else 0 END) as ColumnLevelConst";
            MySql = MySql + ", sum(CASE WHEN ((SK.status & 5 = 5) AND (SK.status & 16=16))	then 1 else 0 END) as HasDefaultValue";
            MySql = MySql + ", sum(CASE WHEN ((SK.status & 4 = 4) AND (SK.status & 16=16))	then 1 else 0 END) as IsCheckConst";
            MySql = MySql + ", sum(CASE WHEN ((SK.status & 3 = 3) AND (SK.status & 16=16))	then 1 else 0 END) as IsForeignKey";
            MySql = MySql + ", sum(CASE WHEN ((SK.status & 2 = 2) AND (SK.status & 32=32))	then 1 else 0 END) as IsUniqueConst";
            MySql = MySql + ", sum(CASE WHEN ((SK.status & 1 = 1) AND (SK.status & 32=32))	then 1 else 0 END) as IsPrimaryKey";
            MySql = MySql + " FROM SYSCONSTRAINTS SK";
            MySql = MySql + " GROUP BY";
            MySql = MySql + " SK.id";
            MySql = MySql + ", SK.colid";
            MySql = MySql + ") AS SKK ";
            MySql = MySql + ") AS SKS ON SKS.id=SIK.id AND SKS.colid=SIK.colid";
            MySql = MySql + " LEFT JOIN sysusers SU ON SO1.uid=SU.uid ";
            MySql = MySql + " WHERE SO1.xtype='U' AND SO1.name='" + MyTabName + "' AND SU.name='" + MySchemaName + "' ORDER BY SCL.colorder, isPrimary DESC, NumeroChiavi"; //</added @MdN: 09/04/2020>
            //MySql = MySql + " WHERE SO1.xtype='U' AND SO1.name='" + MyTabName + "' AND SU.name='" + MySchemaName + "' ORDER BY SCL.colorder";
            //</added @MdN: 19/05/2017>


            if (l_connection.State == ConnectionState.Closed)
                try
                {
                    _trans = null;                  //@MdN 19/05/2016
                    l_connection.Open();
                }
                catch (Exception E)
                {
                    EE = new Spx_ORMMessage("SQLTable.loadInfo(): Unable to open the connection with the catalog.", 0, E);
                    throw EE;
                }

            MyCommand = l_connection.CreateCommand();

            if (_trans != null)                 //@MdN 19/05/2016
                MyCommand.Transaction = _trans; //@MdN 19/05/2016

            MyCommand.CommandText = MySql;
            try
            {
                MyDataR = MyCommand.ExecuteReader();
            }
            catch (Exception E)
            {
                EE = new Spx_ORMMessage("SQLTable.load(): Error executing the query: " + MySql + "\n", 0, E);
                throw EE;
            }

            EE = new Spx_ORMMessage();
            try
            {
                if (MyDataR.HasRows == true)
                {
                    while (MyDataR.Read() == true) // ciclo di lettura.
                    {
                        if (l_ObjectID == null)
                            l_ObjectID = MyDataR.GetString(0);  //prendo l'ID nome della tabella
                        MyName = MyDataR.GetString(MyDataR.GetOrdinal("NomeColonna"));
                        MyType = MyDataR.GetInt16(MyDataR.GetOrdinal("TipoColonna"));
                        MyColumn = new SQLColumn(MyName, MyType);
                        MyColumn.l_HasDefault = MyDataR.GetBoolean(MyDataR.GetOrdinal("hasDefault"));
                        MyColumn.l_IsIdentity = MyDataR.GetBoolean(MyDataR.GetOrdinal("isIdentity"));
                        MyColumn.l_IsPrimayKey = (Boolean)MyDataR.GetBoolean(MyDataR.GetOrdinal("isPrimary"));
                        //<added @MdN: 09/04/2020 - impostazione della dimensione in Byte del campo>
                        MyColumn.l_NBytesColonna = (int)MyDataR.GetInt32(MyDataR.GetOrdinal("ByteColonna"));
                        if (MyColumn.l_IsPrimayKey == true)
                        {
                            if (MyDataR.GetInt16(MyDataR.GetOrdinal("NumeroChiavi")) >= 1)
                            {
                                MyColumn.l_KeyCount = MyDataR.GetInt16(MyDataR.GetOrdinal("NumeroChiavi"));
                                MyColumn.l_IsKeyPart = true;
                                if (MyLostKeyPart == null)
                                    MyLostKeyPart = MyColumn;
                                else
                                {
                                    // concatena le colonne della chiave
                                    //MyColumn.l_LastKeyPart=MyLostKeyPart; //ERRORE
                                    MyLostKeyPart.l_LastKeyPart = MyColumn;
                                    MyLostKeyPart = MyColumn;
                                }

                            }
                        }
                        else
                        {
                            MyColumn.l_KeyCount = 0;
                            MyColumn.l_IsKeyPart = false;
                            MyColumn.l_LastKeyPart = null;
                        }
                        MyColumn.l_IsNullable = MyDataR.GetBoolean(MyDataR.GetOrdinal("isNullable"));
                        //<added @MdN 09/04/2020: NON inserire una colonna già inserita. >
                        if(SQLColumns.Contains(MyColumn.Name)==false)
                            l_Columns.Add(MyColumn); //</added @MdN 09/04/2020>                        
                    }// fine ciclo.
                    MyDataR.Close();
                }
                else
                {
                    EE.store();
                    EE.Message = "SQLTable.loadInfo(): No informations found on table " + l_Name + ".";
                    EE.MessageCode = 5;
                    //throw EE;
                    //EE = new Spx_ORMMessage("SQLTable.load(): table not present in the catalog.", 4);
                }
            }
            catch (Exception E)
            {
                //EE = new Spx_ORMMessage("SQLTable.load(): an error occurs while reading informations from the catalog.", 0, E);
                EE.store();
                EE.Message = "SQLTable.load(): an error occurs while reading informations from the catalog.";
                EE.InnerMessage = (Exception)E;
                EE.MessageCode = 0;
                throw EE;
            }
            if ((EE != null) && (EE.isNew == false))
                throw EE;
        }// fine.

        /// <summary>
        /// Inserisce un nuovo record.
        /// Logica di inserimento:
        ///  Se p_nocheck = false esegue i segenti Controlli:
        ///  Verifica che siano valorizzati:
        ///  - l_loaded = false
        ///  - tutti gli attributi chiave e NON identity 
        ///  - tutti gli attributi NOT NULL che non hanno un valore di default.
        ///  - Restituisce un'eccezione di livello 10 (Spx_ORMMessage)in caso contrario.
        ///  Di conseguenza p_nocheck deve essere normalmente True. Può essere false
        ///  quando l'iserimento è RAW, cioè un semplice accatastamento non controllato di dati.
        ///  <pre>
        ///  ----
        ///  @MdN
        ///  Crtd: 19/05/2016
        ///  </pre>
        /// </summary>
        /// <param name="p_nocheck">Flag che indica se effettuare alcuni controlli di cosistenza: normalmente impostare a true.</param>
        private void insert(Boolean p_nocheck)
        {
            insert(p_nocheck, null);
        }


        /// <summary>
        /// Aggiorna l'oggetto nel database.
        /// <pre>
        /// ----
        /// @MdN 
        /// Crtd: 19/05/2016
        /// </pre>
        /// </summary>
        private void update()
        {
            update(null);
        }

        /// <summary>
        /// Aggiorna l'oggetto nel database.
        /// <pre>
        /// ----
        /// @MdN 
        /// Crtd: 05/08/2014
        /// Mdfd: 12/07/2015 - per evitare conflitti nei nomi di colonna
        /// Mdfd: 19/05/2016 - introduzione della transazione + caricamento dei valori di default come per insert().
        /// Mdfd: 26/01/2020 - Apre la connessione se la trova chiusa e chiude la connessione al termine SE NON C'E' UNA TRANSAZIONE ATTIVA.
        /// Mdfd: 31/01/2020 v. 1.1.0.11: gestisce il caso particolare in cui la CHIAVE COMPOSTA contenga una data.
        /// </pre>
        /// </summary>
        /// <param name="p_trans">Transazione attiva: viene creata e gestita dal chiamante. Può essere null.</param>
        private void update(System.Data.Odbc.OdbcTransaction p_trans)
        {
            Spx_ORMMessage MyMsg = new Spx_ORMMessage();
            StringBuilder MySB = new StringBuilder("UPDATE ").Append(this.l_Name).Append(" SET ");
            StringBuilder MyWhere = new StringBuilder();
            OdbcCommand MyCmd = null;
            // OdbcDataReader MyDR = null;  //<deleted @MdN: 25/01/2020 />
            //SQLColumn MyIdentity = null;  //<deleted @MdN: 25/01/2020 />

            System.Data.Odbc.OdbcTransaction _trans = p_trans;              //@MdN 19/05/2016

            // + Primo controllo:   l'oggetto DEVE essere stato preventivamente caricato e
            //                      e la chiave deve essere tutta specificata.
            if ((this.hasPrimaryFullySpecified() == false) || (this.l_loaded == false))
            {
                MyMsg.Message = "SQLTable.update(): L'oggetto non ha una chiave completamente valorizzata.";
                MyMsg.MessageCode = 9;
                throw MyMsg;
            }
            // ciclo di scansione delle colonne
            bool myIsModified = false;
            bool isFirst = true;
            bool isFirstCondition = true;
            foreach (SQLColumn tmpCol in this.l_Columns)
            {
                /* INSERIMENTO DELLA VIRGOLA */
                if (tmpCol.l_IsKeyPart == true)
                {
                    if (isFirstCondition == false)
                        MyWhere.Append(" AND ");
                    else
                        isFirstCondition = false;
                    //MyWhere.Append(tmpCol.l_Name).Append("=").Append(tmpCol.SQLColumnValue);
                    MyWhere.Append("[").Append(tmpCol.l_Name).Append("]").Append("=").Append(tmpCol.SQLColumnValue);    //@MdN: 12/07/2015 - per evitare conflitti nei nomi di colonna
                    continue;
                }
                else
                    if (tmpCol.l_IsModified == true)
                    {
                        if (isFirst == false)
                            MySB.Append(", ");
                        else
                            isFirst = false;
                        // la colonna viene inserita nella query solo se il suo valore è stato modificato.
                        //MySB.Append(tmpCol.l_Name).Append("=").Append(tmpCol.SQLColumnValue);
                        MySB.Append("[").Append(tmpCol.l_Name).Append("]").Append("=").Append(tmpCol.SQLColumnValue);   //@MdN: 12/07/2015 - per evitare conflitti nei nomi di colonna
                        myIsModified = true;
                    }
            }
            // + Secondo controllo:     la query viene eseguita solo se l'oggetto ha subito qualche
            //                          modifica.
            if (myIsModified == false)
                return;

            // composizione del commando
            if ((this.l_connection.State == ConnectionState.Closed) || (this.l_connection.State == ConnectionState.Broken))
            {
                _trans = null;
                this.l_connection.Open();
            }

            MyCmd = l_connection.CreateCommand();

            if (_trans != null)                     //@MdN 19/05/2016
                MyCmd.Transaction = _trans;         //@MdN 19/05/2016

            MyCmd.CommandText = MySB.Append(" WHERE ").Append(MyWhere.ToString()).ToString();

            // Esecuzione protetta del comando
            try
            {
                if (_trans == null && CurrentConnection.State == ConnectionState.Closed)            //<added @MdN: 26/01/2020 />
                    CurrentConnection.Open();                                                       //<added @MdN: 26/01/2020 />
                MyCmd.ExecuteNonQuery();
            }
            catch (OdbcException SQLE)
            {
                MyMsg.InnerMessage = SQLE;
                MyMsg.Message = "SQLTable.update(): Error in the query [" + MyCmd.CommandText + "]. See inner message.";
                MyMsg.MessageCode = 11;
                throw MyMsg;
            }

            //@MdN 19/05/2016: Caricamento dei valori di default 
            try
            {
                if (_trans != null)         //@MdN 19/05/2016
                    this.load(p_trans);     //@MdN 19/05/2016
                else
                    this.load();
            }
            catch (Spx_ORMMessage EE)
            {
                MyMsg.store();
                MyMsg.Message = "SQLTable.insert() - While loading default values:" + EE.Message + ".\n See inner message.";
                MyMsg.InnerMessage = EE;
                MyMsg.MessageCode = 11;
                throw MyMsg;
            }
            //<added @MdN: 26/01/2020 />
            if (_trans == null && CurrentConnection.State == ConnectionState.Open)
                CurrentConnection.Close();
        }

        /// <summary>
        /// Inserisce un nuovo record.
        /// Logica di inserimento:
        ///  Se p_nocheck = false esegue i segenti Controlli:
        ///  Verifica che siano valorizzati:
        ///  - l_loaded = false
        ///  - tutti gli attributi chiave e NON identity 
        ///  - tutti gli attributi NOT NULL che non hanno un valore di default.
        ///  - Restituisce un'eccezione di livello 10 (Spx_ORMMessage)in caso contrario.
        ///  Di conseguenza p_nocheck deve essere normalmente True. Può essere false
        ///  quando l'iserimento è RAW, cioè un semplice accatastamento non controllato di dati.
        ///  <pre>
        ///  ----
        ///  @MdN
        ///  Mdfd: 12/07/2015 - Evitare conflitti nei nomi di colonna
        ///  Mdfd: 19/05/2016 - introduzione della transazione + caricamento dei valori di default come per insert().
        ///  Mdfd: 26/01/2020 - Apre la connessione se la trova chiusa e chiude la connessione al termine SE NON C'E' UNA TRANSAZIONE ATTIVA.
        ///  </pre>
        /// </summary>
        /// <param name="p_nocheck">Flag che indica se effettuare alcuni controlli di cosistenza: normalmente impostare a true.</param>
        /// <param name="p_trans">Transazione attiva: viene creata e gestita dal chiamante. Può essere null.</param>
        private void insert(Boolean p_nocheck, System.Data.Odbc.OdbcTransaction p_trans)
        {
            //Logica di inserimento:
            //1. Se p_nocheck = false esegue i segenti Controlli:
            //  Verifica che siano valorizzati:
            //  - l_loaded = false
            //  - tutti gli attributi chiave e NON identity 
            //  - tutti gli attributi NOT NULL che non hanno un valore di default.
            //  - Restituisce un'eccezione di livello 10 (Spx_ORMMessage)in caso contrario.
            //3. Esegue l'inserimento;
            //4. Preleva l'eventuale campo IDENTITY;
            //5. ricarica l'oggetto per prelevare gli eventuali valori di default.
            // TEST 03/08/2014: INSERIMENTO CON IDENTITY OK
            // TEST 03/08/2014: INSERIMENTO SENZA IDENTITY OK
            // TEST 03/08/2014: INSERIMENTO SENZA IDENTITY CHIAVE A CAMPI MULTIPLI OK


            Spx_ORMMessage MyMsg = new Spx_ORMMessage();
            StringBuilder MySB = new StringBuilder("INSERT INTO ").Append(this.l_Name).Append("(");
            StringBuilder MyValues = new StringBuilder(" VALUES (");
            OdbcCommand MyCmd = null;
            OdbcDataReader MyDR = null;
            SQLColumn MyIdentity = null;

            System.Data.Odbc.OdbcTransaction _trans = p_trans;              //@MdN 19/05/2016

            // controlli
            // mdfd: @MdN 08/12/2014
            if (p_nocheck == false)
            {   //CONTROLLI PRELIMINARI
                // Se il record esiste --> non si può fare l'inserimento ma l'aggiornamento
                if (l_loaded == true)
                {
                    MyMsg.Message = "SQLTable.insert(): insert operation is impossible. Try to update.";
                    MyMsg.MessageCode = 10;
                    throw MyMsg;
                }
                try
                {
                    verifyForInsert();
                }
                catch (Spx_ORMMessage VFIM)
                {
                    //INTERRUZIONE IMMEDIATA
                    throw VFIM;
                }
            }
            // Ciclo di scrittura della query:
            // nel caso i controlli preliminari venissero bypassati mediante il flag p_nocheck=true,
            // durante questo ciclo vengono ripetuti controlli simili. 
            int t = 0;
            SQLColumn MySQLC = null;
            bool myfirst = true;
            while (t < this.l_Columns.Count)
            {
                MySQLC = (SQLColumn)l_Columns[t];
                // verifica se la colonna deve essere inclusa
                // 1) La colonna non deve essere IDENTITY
                // 2) La colonna deve avere un valore ed essere stata modificata
                // 3) La colonna è stata modificata, non ha un valore espresso e NON ne ammette uno di default --> inserire NULL
                // ---- di conseguenza
                // se una colonna è identity --> SCARTA
                // se una colonna NON è stata modificata --> SCARTA
                // se una colonna è stata modificata, non ha un valore espresso ma ne ammette uno di default --> SCARTA
                // 
                if (MySQLC.l_IsIdentity == false)               // se una colonna è identity --> SCARTA
                {
                    if (MySQLC.l_IsModified == false)
                    {
                        t++;
                        continue;                               //@MdN 08/12/2014: se una colonna NON è stata modificata --> SCARTA                    
                    }
                    if (MySQLC.Value != null)
                    {
                        if (myfirst == false)
                        {
                            MySB.Append(", ");
                            MyValues.Append(", ");
                        }
                        //MySB.Append(MySQLC.Name);
                        MySB.Append("[").Append(MySQLC.Name).Append("]");   // @MdN: 12/07/2015 - Evitare conflitti nei nomi di colonna
                        MyValues.Append(MySQLC.SQLColumnValue);
                        myfirst = false;

                        //if (t < l_Columns.Count - 1)
                        //{
                        //    MySB.Append(", ");
                        //    MyValues.Append(", ");
                        //}
                    }
                    else
                    {
                        // Il campo ha valore null: si deve decidere se saltarlo ...
                        // Il campo ha valore null MA NON ammette valore nullo
                        if (MySQLC.l_IsNullable == false) //@MdN 09/12/2014
                        {
                            if (MySQLC.l_HasDefault == false)
                            {
                                // non ammette valori nulli e non ha valori di default --> ECCEZIONE!!! 
                                MyMsg.Message = MyMsg + " The field " + MySQLC.l_Name + " must have a value.\n";
                            }
                            else
                            {
                                // non ammette valori nulli ma ha valori di default --> SCARTA
                                t++;
                                continue;
                            }
                        }
                        else
                        {
                            // Il campo ha valore null E ammette valore nullo
                            // @MdN 08/12/2014: INSERIRE 'NULL'
                            if (myfirst == false)
                            {
                                MySB.Append(", ");
                                MyValues.Append(", ");
                            }
                            //MySB.Append(MySQLC.Name);
                            MySB.Append("[").Append(MySQLC.Name).Append("]");   // @MdN: 12/07/2015 - Evitare conflitti nei nomi di colonna
                            MyValues.Append("NULL");
                            myfirst = false;
                        }
                    }
                }
                else
                {
                    //memorizzazione la colonna identity
                    MyIdentity = MySQLC;
                }
                t++;
            }
            //chiusura
            MySB.Append(") ");
            MyValues.Append(") ");

            //Composizione della query
            MySB.Append(MyValues.ToString());

            //Esecuzione della query di inserimento
            MyCmd = this.l_connection.CreateCommand();

            if (_trans != null)
                MyCmd.Transaction = _trans;

            MyCmd.CommandText = MySB.ToString();

            try
            {
                //<added @MdN: 26/01/2020 />
                if (CurrentConnection.State == ConnectionState.Closed && _trans == null)        //<added @MdN: 26/01/2020 />
                    CurrentConnection.Open();                                                   //<added @MdN: 26/01/2020 />

                MyCmd.ExecuteNonQuery();

            }
            catch (OdbcException SQLE)
            {
                MyMsg.store();
                MyMsg.Message = "SQLTable.insert() - while inserting NEW record: " + MyCmd.CommandText + ".\n See inner message.";
                MyMsg.InnerMessage = SQLE;
                MyMsg.MessageCode = 11;
                //<added @MdN: 26/01/2020 />
                if (CurrentConnection.State == ConnectionState.Open && _trans == null)              //<added @MdN: 26/01/2020 />
                    CurrentConnection.Close();                                                      //<added @MdN: 26/01/2020 />
                throw MyMsg;
            }

            if (MyIdentity != null)
            {
                MyCmd.CommandText = "SELECT @@IDENTITY";
                try
                {
                    MyDR = MyCmd.ExecuteReader();
                }
                catch (OdbcException SQLE)
                {
                    MyMsg.store();
                    MyMsg.Message = "SQLTable.insert() - while reading IDENTITY value: " + MyCmd.CommandText + ".\n See inner message.";
                    MyMsg.InnerMessage = SQLE;
                    MyMsg.MessageCode = 11;
                    throw MyMsg;
                }
                if (MyDR.HasRows == true)
                {
                    try
                    {
                        MyDR.Read();
                        MyIdentity.l_Value = MyDR.GetInt64(0).ToString();
                    }
                    catch (OdbcException SQLE)
                    {
                        MyMsg.store();
                        MyMsg.Message = "SQLTable.insert() - while reading IDENTITY value: " + MyCmd.CommandText + ".\n See inner message.";
                        MyMsg.InnerMessage = SQLE;
                        MyMsg.MessageCode = 11;
                        throw MyMsg;
                    }
                }
            }
            // + Caricamento dei valori di default
            try
            {
                if (_trans != null)         //@MdN 19/05/2016
                    this.load(p_trans);     //@MdN 19/05/2016
                else
                    this.load();
            }
            catch (Spx_ORMMessage EE)
            {
                MyMsg.store();
                MyMsg.Message = "SQLTable.insert() - While loading default values:" + EE.Message + ".\n See inner message.";
                MyMsg.InnerMessage = EE;
                MyMsg.MessageCode = 11;
                throw MyMsg;
            }

            //<added @MdN: 26/01/2020 />
            if (CurrentConnection.State == ConnectionState.Open && _trans == null)              //<added @MdN: 26/01/2020 />
                CurrentConnection.Close();                                                      //<added @MdN: 26/01/2020 />

            //FINE
        }

        /// <summary>
        /// Verifica se il record che valorizza l'istanza corrente sia presente nel database.
        /// Verifica preliminarmente che la chiave sia valorizzata.
        /// Se la tabella NON ha chiavi, NON ESEGUE ALCUNA VERIFICA.
        /// @MdN 21/01/2014
        /// </summary>
        /// <returns>TRUE: se il record NON esiste o la tabella non ha chiavi, 
        /// FALSE: se il record esiste o non è possibile determinarne l'esistenza.</returns>
        /// <seealso cref="simplex_ORM.Table.hasPrimaryFullySpecified()"/>
        private Boolean checkNotExists()
        {
            return false;
            /*          
                       int t;
                       SQLColumn MyCol;
                       Boolean MyHasPrimary=false;
                       Boolean MyHasKeyFullySpecified = true; //USED?
                       String MySql;
                       StringBuilder MyWhere = new StringBuilder(" WHERE ");
                       // Verifichiamo che la tabella non abbia chiavi o se le ha è completamente 
                       // valorizzata.
                       for (t = 0; t < this.l_Columns.Count; t++)
                       {
                           MyCol = (SQLColumn) l_Columns[t];
                           MyHasPrimary = MyHasPrimary | MyCol.l_IsKeyPart;
                           if(MyCol.l_IsKeyPart==true)
                           {
                               if(MyCol.l_Value==null)
                                   return false;
                               MyWhere.Append(MyCol.Name).Append("=");

                    
                           }
                       }
                       if (MyHasPrimary == false)
                           return true;
                       if(MyHasKeyFullySpecified==false)
                           return false;
            
                       // Verifichiamo che la tabella abbia la chiave completamente specificata.
                       if (base.hasPrimaryFullySpecified() == false)
                           return false;

                       MySql = "SELECT COUNT(*) FROM " + this.l_Name + " WHERE ";
                       for (t = 0; t < this.l_Columns.Count; t++)
                       {
                           MyCol = (SQLColumn)l_Columns[t];
                       }
             */
        }


        #endregion
    }
}
