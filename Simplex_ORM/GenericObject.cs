﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace simplex_ORM
{
    /// <summary>
    /// Classe generica da cui derivano tutte le altre classe quali:
    /// - Columns;
    /// - Table;
    /// </summary>
    public class GenericObject
    {
        private String p_Name;

        /// <summary>
        /// Nome dell'oggetto;
        /// </summary>
        public virtual String Name
        {
            get
            {
                return p_Name;
            }
        }

        /// <summary>
        /// Imposta il nome dell'oggetto.
        /// @MdN: 14/01/2013
        /// </summary>
        /// <param name="_Name">Nome dell'oggetto.</param>
        protected void setName(String _Name)
        {
            p_Name = _Name;
        }
    }
}
