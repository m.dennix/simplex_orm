﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Odbc;
using simplex_ORM;

namespace simplex_ORM.SQLSrv
{
    /// <summary>
    /// Catalogo SQL Server
    /// </summary>
    public class SQLCatalog : Catalog
    {

        ///// <summary>
        ///// Costruttore.
        ///// </summary>
        ///// <param name="p_DbName">Nome Database.</param>
        //public SQLCatalog(String p_DbName)
        //{
        //    l_CataDBName = p_DbName;
        //}

        /// <summary>
        /// Costruttore.
        /// @MdN - 09/12/2013.
        /// </summary>
        /// <param name="p_Str_Connection">Crea un nuovo oggetto Catalogo X Sql Server a partire dalla stringa di connessione.</param>
        public SQLCatalog(String p_Str_Connection)
        {
            l_ConnectionString = p_Str_Connection;
        }

        /// <summary>
        /// Costruttore.
        /// </summary>
        /// <param name="p_DbName">Nome database</param>
        /// <param name="p_StrConnection">Stringa di connessione</param>
        public SQLCatalog(String p_DbName, String p_StrConnection)
        {
            l_CataDBName = p_DbName;
            l_ConnectionString = p_StrConnection;
        }

        /// <summary>
        /// Chiude il catalogo.
        /// </summary>
        public override void closeCatalog()
        {
            ConnectionState tmp=l_st_connection.State;
            if (l_st_connection.State != ConnectionState.Closed)
                l_st_connection.Close();
        }


        /// <summary>
        /// Apre una nuova connessione con il catalogo.
        /// @MdN - 12/12/2013
        /// </summary>
        public override void openCatalog()
        {
            // controlli
            Spx_ORMMessage EE = new Spx_ORMMessage();
            if ((l_ConnectionString == null)) //|| (l_CataDBName ==null))
                throw new Spx_ORMMessage("SQLCatalog.openCatalog(): connection parameters not specified or not valid.", 1);
            try
            {
                if (l_st_connection == null)
                    l_st_connection = new OdbcConnection(l_ConnectionString);
                if (l_st_connection.State == ConnectionState.Closed)
                    l_st_connection.Open();
            }
            catch (Exception E)
            {
                EE.InnerMessage = E;
                EE.Message = "SQLCatalog.openCatalog(): unable to open a connection with the database.";
                EE.MessageCode = 2;
                throw EE;
            }
        }



        /// <summary>
        /// Apertura del catalogo
        /// </summary>
        /// <param name="Str_Connection">Stringa di connessione al database.</param>
        override public void openCatalog(String Str_Connection)
        {
            try{
                this.l_ConnectionString = Str_Connection;
                this.openCatalog();                
                } 
            catch(Spx_ORMMessage E)
            {
                Spx_ORMMessage EE = new Spx_ORMMessage("SQLCatalog.openCatalog(): Can not open the catalog.", 0, E);
                throw EE;
            }
        }
        /// <summary>
        /// Metodo creatore
        /// </summary>
        /// <param name="p_TabName">Nome della tabella nel dtabase di riferimento</param>
        /// <returns>Un oggetto SQLTable</returns>
        public SQLTable createTable(String p_TabName)
        {
            //Controlli
            if ((l_st_connection == null) || (p_TabName == null))
                throw new Spx_ORMMessage("SQLCatalog.crateTable(): the connection does not exists or tble name not specificed.", 1);
            
            if(l_st_connection.State==ConnectionState.Closed)
                throw new Spx_ORMMessage("SQLCatalog.crateTable(): the connection has been closed.", 3);
            
            //Creazione della tabella
            SQLTable MyTable = null;
            try
            {
                MyTable = SQLTable.createSQLTable(l_st_connection, p_TabName);
            }
            catch (Spx_ORMMessage EE)
            {
                EE.store();
                EE.MessageCode = 0;
                EE.Message="SQLCatalog.crateTable(): Generic error, see message's history for details.";
                throw EE;
            }
            return MyTable;
        }

        /// <summary>
        /// Non fa alcuna azione
        /// </summary>
        override public void getTypes()
        {
        }
      
    }
}
