﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.Odbc;
using simplex_ORM;

namespace simplex_ORM.SQLSrv
{
    /// <summary>
    /// Classe che rappresenta la colonna di una tabella 
    /// nella fattispecie SQL Server.
    /// </summary>
    public class SQLColumn : simplex_ORM.Column
    {
        /// <summary>
        /// Tipo Sql Server xtype.
        /// </summary>
        protected int l_xType;

        /// <summary>
        /// Metodo statico che controlla che il parametro p_DateString rappresenti una data nello standard SQL-92
        /// - Dialetto SQL Server: yyyy-MM-dd hh:mm:ss.lll, (lll) sono i millesimi di secondo OPZIONALI in tutto o parzialmente.
        /// 
        /// Invoca l'omonimo metodo statico della classe Columns.
        /// <pre>
        /// ----
        /// @MdN
        /// Crtd: 25/01/2020
        /// </pre>
        /// </summary>
        /// <param name="p_DateString">Stringa nel formato yyyy-MM-dd hh:mm:ss.lll</param>
        /// <returns>True se la verifica è positiva, false altrimeni o in caso di parametro nullo.</returns>
        public static new Boolean isSQLDateString(String p_DateString)
        {
            Char[] _separators = { '.' };
            String[] _components = null;

            if (p_DateString == null)
                return false;

            _components = p_DateString.Split(_separators);
            if (_components.Length > 2)
                return false;

            if (_components.Length == 2)
            {
                if (_components[1].Length > 3)
                    return false;
            }

            return Column.isSQLDateString(_components[0]);
        }

        //internal SQLColumn(OdbcConnection p_MyConn, String p_Name):base()
        //{
        //    Spx_ORMMessage E = new Spx_ORMMessage();

        //    // CONTROLLI
        //    if (p_MyConn == null || p_Name == null)
        //    {
        //        E.Message = "SQLColumn.SQLColumn(): Connection not yet instatied or invalid table name.";
        //        E.MessageCode = 1; //ERRORE
        //        throw E;
        //    }
        //}


        //internal SQLColumn(OdbcDataReader p_MyDataR):base()
        //{
        //    l_Name = p_MyDataR.GetString(p_MyDataR.GetOrdinal("name"));
        //    l_xType = p_MyDataR.GetInt16(p_MyDataR.GetOrdinal("xtype"));
        //    l_SQLType = SQLColumn.mapColumnType(l_xType);

        //}

        /// <summary>
        /// Costruttore
        /// </summary>
        /// <param name="p_Name">Nome della colonna.</param>
        /// <param name="p_xType">Tipo Sql Server della colonna.</param>
        internal SQLColumn(String p_Name, int p_xType):base(p_Name,SQLColumn.mapColumnType(p_xType))
        {
            l_xType = p_xType;            
        }

        /// <summary>
        /// Effettua la corrispondenza tra un tipo SQL Server 2005 - Xtype
        /// ed un tipo SQL standard.
        /// @MdN - 14/12/2013
        /// </summary>
        /// <param name="p_SQLXType">Codice del tipo xtype.</param>
        /// <returns>il codice del tipo standard.</returns>
        internal static int mapColumnType(int p_SQLXType)
        {

            switch (p_SQLXType)
            {
                case 34:    //IMAGE
                case 35:    //TEXT                 
                case 99:    //NTEXT
                case 165:   //VARBINARY
                case 167:   //VARCHAR
                case 173:   //BINARY
                case 231:   //NVARCHAR - SYSNAME
                case 239:   //NCHAR
                case 241:   //XML
                    {
                        return (int)Column_Types.VARCHAR;
                        
                    }
                case 48:    //TINYINT
                case 52:    //SMALLINT
                    {
                        return (int)Column_Types.SMALLINT;
                        
                    }
                case 56: //INT
                    {
                        return (int)Column_Types.INTEGER;
                        
                    }
                case 59: //REAL
                    {
                        return (int)Column_Types.REAL;
                        
                    }
                case 60:    //MONEY
                case 62:    //FLOAT
                case 122:   //SMALLMONEY
                    {
                        return (int)Column_Types.FLOAT;
                        
                    }
                case 58:    //SMALLDATETIME
                case 61:    //DATETIME
                    {
                        return (int)Column_Types.DATE;
                        
                    }
                case 104:    //BIT 
                    {
                        return (int)Column_Types.BIT;
                        
                    }
                case 106:    //DECIMAL
                    {
                        return (int)Column_Types.DECIMAL;
                        
                    }
                case 108:    //NUMERIC
                    {
                        return (int)Column_Types.NUMERIC;
                        
                    }
                case 127:   //BIGINT
                case 187:   //TIMESTAMP
                    {
                        return (int)Column_Types.BIGINT;
                        
                    }
                case 175:   //CHAR
                    {
                        return (int)Column_Types.CHAR;
                        //break; //<deleted @MdN: 25/01/2020 />
                    }//
                default:
                    {
                        // 36: UNIQUE IDENTIFIER
                        // 98: SQL VARIANT
                        return (int)Column_Types.UNKNOWN;                        
                    }
            }
        }
        /// <summary>
        /// Restituisce il tipo T-SQL
        /// </summary>
        /// <param name="p_xType"></param>
        /// <returns></returns>
        internal override Type getColumnType(int p_xType)
        {
            int SqlType;
            SqlType = mapColumnType(p_xType);
            if (SqlType == (int)Column_Types.UNKNOWN)
            {
                switch (p_xType)
                {
                    case 36:
                        {
                            return new System.IntPtr().GetType();
                        }
                    case 98:
                        {
                            return Type.GetType("System.Object");
                        }
                    default:
                        {
                            return Type.GetType("System.Object");
                        }
                }
            }
            return base.getColumnType(SqlType);
        }// fine

        /// <summary>
        ///Restituisce il nome della colonna.
        ///(RO)
        /// @MdN 16/07/2014
        /// </summary>
        public override String Name
        {
            get
            {
                return base.l_Name;
            }
        }

        /// <summary>
        ///Restituisce il valore della colonna.
        ///(RO)
        /// @MdN 16/07/2014
        /// </summary>
        public String Value
        {
            get
            {
                return base.l_Value;
            }
        }

        /// <summary>
        /// Indica se la colonna ha subito una modifica dall'ultimo caricamento (load()).
        /// <pre>
        /// ----
        /// @MdN
        /// Crtd: 10/04/2020
        /// </pre>
        /// </summary>
        public Boolean isModified
        {
            get
            {
                return base.l_IsModified;
            }
        }
    }
    
}