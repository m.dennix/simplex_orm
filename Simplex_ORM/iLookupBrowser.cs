﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace simplex_ORM
{
    /// <summary>
    /// <p>
    /// Interfaccia dell'oggetto LookupBrowser che consente di ottenere, data una chiave,
    /// il relativo valore.
    /// </p>
    /// <pre>
    /// ----
    /// @MdN
    /// Crtd: 08/08/2015
    /// </pre>
    /// </summary>
    public interface iLookupBrowser
    {
        /// <summary>
        /// <p>
        /// Dato il valore della chiave come intero, restituisce un valore come stringa.
        /// </p>
        /// <pre>
        /// ----
        /// @MdN
        /// Crtd: 08/08/2015
        /// </pre>
        /// </summary>
        /// <param name="p_key">Chiave</param>
        /// <returns>Valore</returns>
        String getValue(Int32 p_key);

        /// <summary>
        /// <p>
        /// Dato il valore della chiave come stringa, restituisce un valore come stringa.
        /// </p>
        /// <pre>
        /// ----
        /// @MdN
        /// Crtd: 08/08/2015
        /// </pre>
        /// </summary>
        /// <param name="p_key">Chiave</param>
        /// <returns>Valore</returns>
        String getValue(String p_key);
    }
}
