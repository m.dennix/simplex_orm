﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// Le informazioni generali relative a un assembly sono controllate dal seguente 
// insieme di attributi. Per modificare le informazioni associate a un assembly
// occorre quindi modificare i valori di questi attributi.
[assembly: AssemblyTitle("Simplex_ORM")]
[assembly: AssemblyDescription("+ PrimaryKeyColumns + Continue +MaxLength + Bit Column ;  @MdN 19/05/2016 -Introduzione delle transazioni nei metodi SQLTable + nuovi metodi per SQLTable + Mdfd: 11/10/2017 - SQLForeignKey.load(): corretto BUG sulla chiusura della connessione. + 25/01/2020 isSQLDateString()")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("@MdN - 15/11/2015; 19/05/2016; 19/05/2017; 01/10/2017; 10/01/2018; 14/07/2018; 25/01/2020")]
[assembly: AssemblyProduct("Simplex_ORM")]
[assembly: AssemblyCopyright("Copyright ©  2013 - 2020")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Se si imposta ComVisible su false, i tipi in questo assembly non saranno visibili 
// ai componenti COM. Se è necessario accedere a un tipo in questo assembly da 
// COM, impostare su true l'attributo ComVisible per tale tipo.
[assembly: ComVisible(false)]

// Se il progetto viene esposto a COM, il GUID che segue verrà utilizzato per creare l'ID della libreria dei tipi
[assembly: Guid("26248576-d6c9-46f9-a9fb-a49ac1dd741e")]

// Le informazioni sulla versione di un assembly sono costituite dai seguenti quattro valori:
//
//      Numero di versione principale
//      Numero di versione secondario 
//      Numero build
//      Revisione
//
// È possibile specificare tutti i valori oppure impostare i valori predefiniti per i numeri relativi alla build e alla revisione 
// utilizzando l'asterisco (*) come descritto di seguito:
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("1.1.0.12")]
[assembly: AssemblyFileVersion("1.1.0.12")]
