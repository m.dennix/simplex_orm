﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.Generic;
using System.Collections;
using System.Data.Odbc;
using System.Data;

namespace simplex_ORM
{
    /// <summary>
    /// Classe che rappresenta una tabella.
    /// @MdN 14/01/2012
    /// </summary>
    public class Table
    {
        protected String l_Name;
        protected ArrayList l_Columns;

        protected OdbcConnection l_connection;

        /// <summary>
        /// Nome della tabella.
        /// </summary>
        public String Name
        {
            get
            {
                return l_Name;
            }
        }

        /// <summary>
        /// Dato un valore Datetime, restituisce la corrispondente stringa col seguente formato SQL:
        /// "YYYY-MM-DD[spazio]hh:mm:ss"
        /// @MdN - 10/01/2014
        /// </summary>
        /// <param name="p_date">Data da trasformare</param>
        /// <returns>stringa di formato "YYYY-MM-DD[spazio]hh:mm:ss"</returns>
        public static String DateToSQLDateString(DateTime p_date)
        {
            return p_date.ToString("yyyy-MM-dd HH:mm:ss");            
        }

        /// <summary>
        /// Data una stringa nel formato "yyyy-MM-dd HH:mm:ss", ricava un valore di tipo DateTime
        /// @MdN - 10/01/2014
        /// </summary>
        /// <param name="p_SQLDate">Stringa nel formato "yyyy-MM-dd HH:mm:ss".</param>
        /// <returns>Il valore Datetime corrispondente alla stringa passata come parametro.</returns>
        public static DateTime SQLDateStringToDate(String p_SQLDate)
        {
            DateTime.t
        }
       
    }
}
