﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using System.Data.Odbc;
using System.Data;

namespace simplex_ORM
{
    /// <summary>
    /// Classe che rappresenta una tabella.
    /// @MdN 14/01/2012
    /// </summary>
    public class Table
    {
        /// <summary>
        /// <p>Nome della tabella.</p>
        /// </summary>
        protected String l_Name;
        /// <summary>
        /// <p>Lista delle colonne che compongono la tabella.</p>
        /// </summary>
        protected ArrayList l_Columns;
        /// <summary>
        /// <p>Connessione ODBC.</p>
        /// </summary>
        /// <remarks>DEVE ESSERE APERTA E CHIUSA DALLA PROCEDURA CHIAMANTE.</remarks>
        protected OdbcConnection l_connection;
        /// <summary>
        /// indica che l'oggetto è stato caricato. Per default è false ad indicare che l'oggetto rappresenta un nuovo record.
        /// </summary>
        protected Boolean l_loaded;
        /// <summary>
        /// <p>indica che l'oggetto è stato modificato attraverso i metodi di impostazione dei valori che devono essere definiti nelle classi dervate.</p>
        /// </summary>
        protected Boolean l_changed;
                       
        /// <summary>
        /// Nome della tabella.
        /// </summary>
        /// <remarks>R.O.</remarks>
        public String Name
        {
            get
            {
                return l_Name;
            }
        }

        /// <summary>
        /// Verifica che la chiave primaria esista e sia valorizzata.
        /// @MdN - 14/01/2014
        /// </summary>
        /// <returns>True se la chiave esiste ed è completamente valorizzata, False altrimenti.</returns>
        public Boolean hasPrimaryFullySpecified()
        {
            if (l_Columns == null)
                return false;
            Column MyTmpCol = null;
            Boolean toRet = true;
            Boolean PrimaryFound = false;
            //Ciclo di ricerca
            for (int t = 0; t < l_Columns.Count; t++)
            {
                MyTmpCol = (Column)l_Columns[t];
                if (MyTmpCol.l_IsPrimayKey == true)
                {
                    PrimaryFound = true;
                    if (MyTmpCol.l_Value != null)
                        toRet = toRet & true;
                    else
                    { //@MdN - 07/12/2014: SE LA CHIAVE HA UNA COMPONENTE
                      //NULLA MA HA UN VALORE DI DEFAUL --> E' COME SE FOSSE VALORIZZATA!
                        if(MyTmpCol.l_HasDefault==true)
                            toRet = toRet & true;
                        else
                            toRet = toRet & false;
                    } //@MdN - 07/12/2014
                }
            }
            toRet = toRet & PrimaryFound;
            return toRet;
        }
    }
}
