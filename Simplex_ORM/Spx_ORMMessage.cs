﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace simplex_ORM
{
    /// <summary>
    /// <p>
    /// Messaggio per comunicare situazioni anomale.
    /// <ul>
    /// <li>0:  Messaggio generico. Vedere il testo del messaggio. / Generic Message. See the text of the message.</li>
    /// <li>1:  Parametri non validi o non specificati. / Invalid or not specified parameters.</li>
    /// <li>2:  Impossibile aprire una connessione con il database. / Unable to establish a connection with the database.</li>
    /// <li>3:  Connessione chiusa. / Closed connection.</li>
    /// <li>4:  Tabella non presente nel catalogo. / The specified table does not exists.</li>
    /// <li>5:  Informazioni sulla tabella specificata assenti. / Informations on the specified table are unavailable.</li>
    /// <li>6:  La colonna non ammette valori nulli. / The specified column does not allow null values.</li>
    /// <li>7:  La cononna specificata non esiste nella tabella. / The specified column does not exist in the specified table.</li>
    /// <li>8:  Non è stato possibile trovare il record specificato. / Unable to find the specified record.</li>
    /// <li>9:  Chiave primaria non completamente valorizzata. / Primary key not fully specified.</li>
    /// <li>10: Impossibile inserire il record. / Unable to insert the record.</li>
    /// <li>11: Errore nell'esecuzione della query. / Query error.</li>
    /// <li>12: Tipo non corrispondente. / Type mismatch.</li>
    /// </ul>
    /// </p>
    /// <pre>
    /// ----
    /// @MdN
    /// Crtd: 1.0.0.0 ???
    /// Mdfd: 1.0.0.1: 10/01/2018 - Introdotto in traceLog() il troncamento del testo del messaggio con controllo preliminare della presenza del doppio apice
    /// Mdfd: 1.0.0.2: 14/07/2018 - (simplex_ORM.dll 1.1.0.9) Introdotto l'enumeratore MESSAGE_TYPE.
    /// Mdfd: 1.0.0.3: 14/04/2020 - (simplex_ORM.dll 1.1.0.12) Introdotta la property GET/SET che esegue l'override di Source.
    /// </pre>
    /// </summary>
    public class Spx_ORMMessage: Exception
    {
        #region ATTRIBUTI PRIVATI / PROTETTI
        /// <summary>
        /// Corpo del messaggio.
        /// </summary>
        private String l_Message = null;
        /// <summary>
        /// Codice del messaggio
        /// </summary>
        private int l_MessageCode = 0;
        /// <summary>
        /// Se il messaggio deriva da un'eccezione catturata indica 
        /// il riferimento a tale eccezione.
        /// </summary>
        private Exception l_InnerMessage = null;
        /// <summary>
        /// Nel caso di messaggi concatenati, è il riferimento al messaggio successivo.
        /// </summary>
        private Spx_ORMMessage l_Next=null;
        /// <summary>
        /// Indica se il messaggio è nuovo e deriva da modifiche di un messaggio precedente.
        /// </summary>
        private Boolean l_new = true;
        /// <summary>
        /// Campo ausiliario di testo libero che consente di specificare l'oggetto che 
        /// su cui si vuol far focalizzare l'attenzione, come ad esempio il nome di una
        /// colonna o di una tabella.
        /// ----
        /// @MdN 
        /// Crtd: 04/02/2014
        /// </summary>
        private String l_MessageField = null;

        /// <summary>
        /// Lunghezza massima di un messaggio da scrivere nel log (SUL DATABASE è 1024)
        /// <pre>
        /// ----
        /// @MdN
        /// Crtd: 10/01/2018
        /// </pre>
        /// </summary>
        protected static int MAX_LOGMESSAGE_LENGTH = 1000;

        /// <summary>
        /// Tipo di questo messaggio. 
        /// <ol>
        /// <li>MESSAGGIO SEMPLICE      (0: SIMPLE_MSG)</li>
        /// <li>MESSAGGIO WARNING       (1: WARNING_MSG)</li>
        /// <li>MESSAGGIO D'ERRORE      (2: ERROR_MSG)</li>
        /// <li>MESSAGGIO INFORMATIVO   (3: INFO_MSG)</li>
        /// <li>MESSAGGIO DATABASE      (4: DATABASE_MSG) </li>
        /// </ol><br></br>
        /// <strong>Attenzione: dalla versione 1.0.0.2 per il valore fare il cast dell'enumerazione MESSAGE_TYPE.</strong>
        /// Per i codici superiori a "4" la codifica è libera.
        /// ----
        /// @MdN 
        /// Crtd: 04/02/2014
        /// Mdfd: @MdN 25/06/2015: LOGTYPE=2
        /// </summary>
        private int l_TraceType=2;
        #endregion

        #region ATTRIBUTI PUBBLICI
        /// <summary>
        /// Tipologie possibili di messaggio.<br></br>
        /// <ol>
        /// <li>MESSAGGIO SEMPLICE      (0: SIMPLE_MSG)</li>
        /// <li>MESSAGGIO WARNING       (1: WARNING_MSG)</li>
        /// <li>MESSAGGIO D'ERRORE      (2: ERROR_MSG)</li>
        /// <li>MESSAGGIO INFORMATIVO   (3: INFO_MSG)</li>
        /// <li>MESSAGGIO DATABASE      (4: DATABASE_MSG)</li>
        /// </ol><br></br>
        /// <pre>
        /// ----
        /// @MdN
        /// Crtd: 14/07/2018 (da GenericSpxForm)
        /// </pre>
        /// </summary>
        public enum MESSAGE_TYPE { 
            /// <summary>
            /// SIMPLE_MSG (0)
            /// </summary>
            SIMPLE_MSG, 
            /// <summary>
            /// WARNING_MSG (1)
            /// </summary>
            WARNING_MSG, 
            /// <summary>
            /// ERROR_MSG (2)
            /// </summary>
            ERROR_MSG, 
            /// <summary>
            /// INFO_MSG (3)
            /// </summary>
            INFO_MSG, 
            /// <summary>
            /// DATABASE_MSG (4)
            /// </summary>
            DATABASE_MSG };
        #endregion

        #region PROPERTY PUBBLICHE
        /// <summary>
        /// Storicizza il messaggio in una coda e svuota l'oggetto.
        /// Se il messaggio corrente è vuoto, esce.
        /// @MdN - 14/12/2013
        /// </summary>
        /// <seealso cref="ORMMessages()"/>
        /// 
        public void store()
        {
            //Preparazione della copia
            Spx_ORMMessage tmp = new Spx_ORMMessage();
            if (l_Message != null)
            {
                tmp.l_Message = l_Message;
                l_Message = null;
            }
            else
                return;
            
            if (l_MessageCode != 0)
            {
                tmp.l_MessageCode = l_MessageCode;
                l_MessageCode = 0;
            }

            if (l_InnerMessage != null)
            {
                tmp.l_InnerMessage = l_InnerMessage;
                l_InnerMessage = null;
            }
            //Accodamento "in testa" della copia
            if (l_Next != null)
                tmp.l_Next = l_Next;
            l_Next = tmp;
        }

        /// <summary>
        /// REstituisce il numero di messaggi accodati nell'oggetto corrente.
        /// @MdN - 14/12/2013
        /// </summary>
        public int ORMMessages
        {
            get
            {
                Spx_ORMMessage cur = null;
                int tmp;
                if (l_new == true)
                    return 0;
                else
                    tmp = 1;        //considero anche il corrente!
                   
                cur = l_Next;
                while (cur != null)
                {
                    tmp++;
                    cur = cur.l_Next;
                }
                return tmp;
            }
        }

        /// <summary>
        /// Indica se l'istanza è nuova.
        /// @MdN - 19/12/2013
        /// </summary>
        public Boolean isNew
        {
            get
            {
                return l_new;
            }
        }

        /// <summary>
        /// Restituisce ed imposta il testo che costituisce il messaggio.
        /// @MdN 09/12/2013
        /// </summary>
        new public String Message
        {
            get
            {
                return l_Message;
            }
            set
            {
                if (ORMMessages == 0)
                    l_new = false;
                l_Message = value;
            }
        }

        /// <summary>
        /// Restituisce ed imposta il codice del messaggio.
        /// @MdN 09/12/2013
        /// </summary>
        public int MessageCode
        {
            get
            {
                return l_MessageCode;
            }
            set
            {
                l_MessageCode = value;
            }
        }

        /// <summary>
        /// Imposta e restituisce il messaggio dell'eccezione precedente.
        /// @MdN 09/12/2013.
        /// </summary>
        public Exception InnerMessage
        {
            get
            {
                return l_InnerMessage;
            }
            set
            {
                l_InnerMessage = value;
            }
        }

        /// <summary>
        /// Imposta e/o restituisce (RW) il campo ausiliario del messaggio.
        /// @MdN
        /// ----
        /// Crtd: 04/02/2015
        /// </summary>
        public String MessageField
        {
            get
            {
                return l_MessageField;
            }
            set
            {
                l_MessageField = value;
            }
        }

        /// <summary>
        /// Imposta e/o restituisce la sorgente dell'errore. Se l'attributo MessageField è stato popolato, restituisce la concatenazione
        /// MessageField-->Source.
        /// ----
        /// @MdN
        /// Crtd: 14/04/2020
        /// </summary>
        public override string Source
        {
            get
            {
                if(MessageField==null)
                    return base.Source;
                return MessageField + "-->" + base.Source;
            }
            set
            {
                base.Source = value;
            }
        }

        /// <summary>
        /// <p>
        /// Imposta o ottiene (RW) il tipo di messaggio. <br></br>
        /// Il valore di default è 2: MESSAGGIO D'ERRORE.
        /// </p>
        ///  
        /// <ol>
        /// <li>MESSAGGIO SEMPLICE</li>
        /// <li>MESSAGGIO DATABASE</li>
        /// <li>MESSAGGIO D'ERRORE</li>
        /// </ol>
        /// Per i codici superiori a "2" la codifica è libera.
        /// ----
        /// @MdN 
        /// Crtd: 25/06/2015
        /// 
        /// </summary>        
        public int MessageType
        {
            get
            {
                return l_TraceType;
            }
            set
            {
                l_TraceType = value;
            }

        }

        /// <summary>
        /// Costruttore.
        /// </summary>
        public Spx_ORMMessage()
        {
            l_MessageCode = 0;
            l_InnerMessage = null;
            l_Message = null;
        }
        #endregion

        #region COSTRUTTORI
        /// <summary>
        /// Costruttore.
        /// <pre>
        /// ----
        /// @MdN
        /// Mdfd: 12/07/2015 - Il testo iniziale del messaggio coincide con il testo dell'eccezione.
        /// Mdfd: 12/07/2015 - Il testo iniziale del campo MessageField è la proprietà Source dell'eccezione.
        /// </pre>
        /// </summary>
        /// <param name="p_E">Costruisce un messaggio sulla base di un'eccezione.</param>
        public Spx_ORMMessage(Exception p_E)
        {
            l_MessageCode = 0;
            l_InnerMessage = p_E;
            l_Message = p_E.Message;            //@MdN: 12/07/2015 - Il testo iniziale del messaggio coincide con il testo dell'eccezione.
            l_MessageField = p_E.Source;        //@MdN: 12/07/2015 - Il testo iniziale del campo MessageField è la proprietà Source dell'eccezione.
        }

        /// <summary>
        /// Costruttore.
        /// </summary>
        /// <param name="p_Message">Descrizione del messaggio.</param>
        public Spx_ORMMessage(String p_Message)
        {
            l_MessageCode = 0;
            l_Message = p_Message;
        }
        /// <summary>
        /// Costruttore.
        /// </summary>
        /// <param name="p_Message">Descrizione del messaggio.</param>
        /// <param name="p_Code">Codice del messaggio.</param>
        public Spx_ORMMessage(String p_Message, int p_Code)
        {
            l_MessageCode = p_Code;
            l_Message = p_Message;
        }

        /// <summary>
        /// Costruttore.
        /// </summary>
        /// <param name="p_Message">Descrizione del messaggio.</param>
        /// <param name="p_Code">Codice del messaggio.</param>
        /// <param name="p_E">Eccezione originario.</param>
        public Spx_ORMMessage(String p_Message, int p_Code, Exception p_E)
        {
            l_MessageCode = p_Code;
            l_Message = p_Message;
            l_InnerMessage = p_E;
        }

        /// <summary>
        /// Costruttore.
        /// </summary>
        /// <param name="p_Message">Descrizione del messaggio.</param>
        /// <param name="p_Code">Codice del messaggio.</param>
        /// <param name="p_Field">Campo ausiliario del messaggio.</param>
        /// <param name="p_E">Eccezione originario.</param>
        public Spx_ORMMessage(String p_Message, int p_Code, String p_Field, Exception p_E)
        {
            l_MessageCode = p_Code;
            l_Message = p_Message;
            l_InnerMessage = p_E;
            l_MessageField = p_Field;
        }

        /// <summary>
        /// Costruttore
        /// </summary>
        /// <param name="p_Code">Codice del Messaggio</param>
        public Spx_ORMMessage(int p_Code)
        {
            l_MessageCode = p_Code;
        }

        /// <summary>
        /// Costruttore
        /// </summary>
        /// <param name="p_Field">Campo ausiliario del messaggio.</param>
        /// <param name="p_Code">Codice del Messaggio</param>
        public Spx_ORMMessage(int p_Code, String p_Field)
        {
            l_MessageCode = p_Code;
            l_MessageField = p_Field;
        }

        /// <summary>
        /// Costruttore
        /// <pre>
        /// @MdN
        /// ----
        /// Crtdd:19/04/215
        /// </pre>
        /// </summary>
        /// <param name="p_Code">Codice dell'errore</param>
        /// <param name="p_Field">Campo o oggetto che ha scatenato l'errore</param>
        /// <param name="p_Message">Messaggio d'errore</param>
        public Spx_ORMMessage(int p_Code, String p_Field, String p_Message)
        {
            l_Message = p_Message;
            l_MessageCode = p_Code;
            l_MessageField = p_Field;
        }


        /// <summary>
        /// Costruttore
        /// </summary>
        /// <param name="p_Field">Campo ausiliario del messaggio.</param>
        /// <param name="p_Code">Codice del Messaggio.</param>
        /// <param name="p_E">Eccezione da cui deriva il messaggio.</param>
        public Spx_ORMMessage(int p_Code, String p_Field, Exception p_E)
        {
            l_MessageCode = p_Code;
            l_MessageField = p_Field;
            l_InnerMessage = p_E;
        }

        /// <summary>
        /// Costruttore.
        /// </summary>
        /// <param name="p_Message">Descrizione del messaggio.</param>
        /// <param name="p_Field">Campo ausiliario del messaggio.</param>
        public Spx_ORMMessage(String p_Message, String p_Field)
        {
            l_MessageCode = 0;
            l_Message = p_Message;
            l_MessageField = p_Field;
        }
        #endregion

        #region METODI PUBBLICI
        /// <summary>
        /// Produce una stringa multiriga contenente la storia delle eccezioni catturate.
        /// @MdN - 19/12/2013
        /// </summary>
        /// <returns>Storia delle eccezioni catturate.</returns>
        public override string ToString()
        {
            Spx_ORMMessage myTmp = null;
            StringBuilder MyOut = new StringBuilder();
            int MyRow =0;
            MyOut.Append(MyRow.ToString()).Append("\t").Append(MessageCode.ToString()).Append(" ").AppendLine(Message);
            if (InnerMessage != null)
                MyOut.Append("\t\t").AppendLine(InnerMessage.Message);
            MyRow++;
            myTmp = l_Next;
            while (myTmp != null)
            {
                MyOut.Append(MyRow.ToString()).Append("\t").Append(myTmp.MessageCode.ToString()).Append(" ").AppendLine(myTmp.Message);
                if (myTmp.InnerMessage!= null)
                    MyOut.Append("\t\t").AppendLine(myTmp.InnerMessage.Message);
                MyRow++;
                myTmp = myTmp.l_Next;                
            }
            MyOut.AppendLine("------");
            return MyOut.ToString();
        }

        /// <summary>
        /// <p>
        /// Scrive un messaggio nella tabella dei log.
        /// Se la connessione attiva è chiusa, la apre automaticamente e la richiude in uscita.
        /// La tabella dei log di default, dbo.UTILS_LOGS ha la seguente struttura:
        /// </p>
        /// <pre>
        /// ID                      INT,
        /// SOURCE                  NVARCHAR(1024),
        /// LOGMESSAGE              NVARCHAR(1024),
        /// LOGTYPE                 INT,
        /// LOGCODE                 INT,
        /// DATA_SYS                DATETIME,
        /// USR                     INT,
        /// [SESSIONID]             NVARCHAR(256)
        /// </pre>
        /// Il significato dei campi è il seguente
        /// <UL>
        /// <li>ID:                 IDENTIFICATIVO SEQUENZIALE</li>
        /// <li>SOURCE:             DOVE VIENE ORIGINATO IL MESSAGGIO (OGGETTO, CATENA BREADCRUMB, METODO, ETC.). L'attributo l_MessageField viene inserito in questo campo.</li>
        /// <li>LOGMESSAGE:         E' IL TESTO DEL MESSAGGIO. PUO' ESSERE LA DESCRIZIONE DI UN ERRORE, UNA QUERY, UN ELENCO DI VALORI, ETC</li>
        /// <li>LOGTYPE:            E' LA CODIFICA DEL MESSAGGIO. 0=MESSAGGIO SEMPLICE, 1=MESSAGGIO DATABASE, 2=MESSAGGIO ERRORE, 3....</li>
        /// <li>LOGCODE             E' IL CODICE DEL MESSAGGIO</li>
        /// <li>DATA_SYS:           DATA del sistema</li>
        /// <li>USR:                UTENTE CORRENTE</li>
        /// <li>[SESSION ID]:       ID della sessione</li>
        /// </UL>
        /// <pre>
        /// @MdN
        /// ----
        /// Crtd: 06/02/2015
        /// Mdfd: 07/09/2015 - Corretto il bug sugli apostofi.
        /// Mdfd: 10/01/2018 - Introdotto il troncamento del testo del messaggio con controllo preliminare della presenza del doppio apice
        /// Mdfd: 09/09/2018 - introdotti alcuni controlli sui parametri. In questo modo non sono più necessari i controlli preliminari.
        /// Mdfd: 14/04/2020 - Modificata la parte della query di inserimento del valore SOURCE.
        /// </pre> 
        /// </summary>
        /// <param name="p_Conn">Connessione attiva</param>
        /// <param name="p_UserID">Id dell'utente corrente.</param>
        /// <param name="P_SessionId">Id della sessione corrente.</param>
        /// <returns>True in caso di scrittura corretta, false altrimenti.</returns>
        public virtual bool traceLog(System.Data.Odbc.OdbcConnection p_Conn, String p_UserID, String P_SessionId)
        {
            System.Text.StringBuilder _Sql=new StringBuilder("INSERT INTO dbo.UTILS_LOGS(SOURCE, LOGMESSAGE,  LOGCODE, LOGTYPE, USR, SESSIONID) VALUES(");
            System.Data.Odbc.OdbcCommand _cmd=null;
            String _MessageToWrite = null;
            //controlli preliminari            
            if (p_Conn == null)
                return false;
            if (p_Conn.State == System.Data.ConnectionState.Closed)
                p_Conn.Open();

            //<added @MdN: 09/09/2018>
            //<deleted @MdN: 25/01/2020 />
            //if (p_UserID == null)
            //    p_UserID = "unknown";
            //<deleted @MdN: 25/01/2020 />

            if (P_SessionId == null)
                P_SessionId = "none";
            //</added @MdN: 09/09/2018>
            
            //l_Message_Field-->SOURCE
            //<deleted @MdN: 14/04/2020> if (this.l_MessageField != null)</deleted>
            if (this.Source!= null)
            {
                //<added @MdN: 10/01/2017>
                //<deleted @MdN: 14/04/2020> _MessageToWrite = l_MessageField.Replace("'", "''");                                        // raddoppio gli apici </deleted>
                //<added @MdN: 14/04/2020>
                _MessageToWrite = this.Source;
                _MessageToWrite = this.Source.Replace("'", "''");

                if (_MessageToWrite.Length > Spx_ORMMessage.MAX_LOGMESSAGE_LENGTH)                          // TRONCAMENTO: se la stringa con gli apici raddoppiati ha lunghezza maggiore di 1000 (il max è 1024)
                {                                                                                           // controllo che il troncamento NON separi un apice raddoppiato, altrimenti si genera un errore SQL.
                    if (_MessageToWrite.LastIndexOf("''", Spx_ORMMessage.MAX_LOGMESSAGE_LENGTH - 2) == Spx_ORMMessage.MAX_LOGMESSAGE_LENGTH - 1)
                        _MessageToWrite = _MessageToWrite.Substring(0, Spx_ORMMessage.MAX_LOGMESSAGE_LENGTH - 3)+ "..."; // col troncamento tolgo l'apice raddoppiato
                    else
                        _MessageToWrite = _MessageToWrite.Substring(0, Spx_ORMMessage.MAX_LOGMESSAGE_LENGTH - 1) + "..."; // TRONCAMENTO
                }
                //<added @MdN: 10/01/2017>                
                //</added @MdN: 14/04/2020>
                //_Sql.Append("'").Append(l_MessageField.Replace("'", "''")).Append("', "); //<-- @MdN 07/09/2015
                _Sql.Append("'").Append(_MessageToWrite).Append("', "); //<-- @MdN 10/01/2018
            }
            else
                _Sql.Append("NULL, ");

            // l_Message -->LOGMESSAGE
            if (this.l_Message != null)
            {
                //<added @MdN: 10/01/2017>
                _MessageToWrite = l_Message.Replace("'", "''");                                             // raddoppio gli apici
                if (_MessageToWrite.Length > Spx_ORMMessage.MAX_LOGMESSAGE_LENGTH)                          // TRONCAMENTO: se la stringa con gli apici raddoppiati ha lunghezza maggiore di 1000 (il max è 1024)
                {                                                                                           // controllo che il troncamento NON separi un apice raddoppiato, altrimenti si genera un errore SQL.
                    if (_MessageToWrite.LastIndexOf("''", Spx_ORMMessage.MAX_LOGMESSAGE_LENGTH - 2) == Spx_ORMMessage.MAX_LOGMESSAGE_LENGTH - 1)
                        _MessageToWrite = _MessageToWrite.Substring(0, Spx_ORMMessage.MAX_LOGMESSAGE_LENGTH - 3) + "..."; // col troncamento tolgo l'apice raddoppiato
                    else
                        _MessageToWrite = _MessageToWrite.Substring(0, Spx_ORMMessage.MAX_LOGMESSAGE_LENGTH - 1) + "..."; // TRONCAMENTO
                }
                //<added @MdN: 10/01/2017>                
                //_Sql.Append("'").Append(l_MessageField.Replace("'", "''")).Append("', "); //<-- @MdN 07/09/2015
                _Sql.Append("'").Append(_MessageToWrite).Append("', "); //<-- @MdN 10/01/2018
            }
            else
                _Sql.Append("NULL, ");

            // l_MessageCode -->LOGCODE
            _Sql.Append(l_MessageCode.ToString()).Append(", ");
            
            // l_TraceType --> LOGTYPE
            _Sql.Append(l_TraceType.ToString()).Append(", ");

            //USR (okkio che e' previsto un intero ma il parametro è una stringa!!!!)
            if(p_UserID!=null)
                 _Sql.Append("'").Append(p_UserID).Append("', ");
            else
                _Sql.Append("NULL, ");
            // SESSION ID
            if(P_SessionId!=null)
                 _Sql.Append("'").Append(P_SessionId).Append("')");
            else
                _Sql.Append("NULL)");

            _cmd = p_Conn.CreateCommand();
            _cmd.CommandText = _Sql.ToString();
            try
            {
                _cmd.ExecuteNonQuery();
            } catch(Exception)
            {
                return false;
            }
            _cmd.Dispose();
            p_Conn.Close();
            return true;
        }//fine
        #endregion

    }
}
