﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using simplex_ORM;
using System.Data.Odbc;

namespace simplex_ORM.SQLSrv
{
    /// <summary>
    /// <p>
    /// IT: Classe che rappresenta un vincolo di integrità referenziale.
    /// </p>
    /// <br>
    /// La classe è priva di un costruttore pubblico e le sue istanze possono essere
    /// ottenute solamente mediante il metodo factory <b><i>getForeignKey(String)</i></b> di un'istanza di
    /// un oggetto di classe SQLTable.
    /// </br>
    /// <pre>
    /// ----
    /// @MdN
    /// Crtd: 02/05/2015
    /// Mdfd: 11/10/2017 - load(): corretto BUG sulla chiusura della connessione (simplex_ORM v.1.1.0.7). 
    /// </pre>
    /// </summary>
    public class SQLForeignKey
    {
        /// <summary>
        /// Tabella referenziata
        /// </summary>
        internal SQLTable MyReferenced = null;
        /// <summary>
        /// Tabella referente
        /// </summary>
        internal SQLTable MyReferencing = null;
        /// <summary>
        /// Insieme di chiavi esterne della tabella referenziata
        /// </summary>
        internal String[] MyReferencedKeys = null;
        /// <summary>
        /// Insieme di chiavi candidate della tabella referente
        /// </summary>
        internal String[] MyReferencingKeys = null;
        /// <summary>
        /// Nome del vincolo
        /// </summary>
        internal String MyName = null;
        /// <summary>
        /// ID del vincolo (prelevato da sysobjects)
        /// </summary>
        internal Int32 MyID = 0;
        /// <summary>
        /// Numero di colonne costituenti il vincolo.
        /// </summary>
        internal int MyNumCols = 0;
        /// <summary>
        /// Connessione attiva. Viene testato lo stato prima dell'uso.
        /// Viene chiusa al termine dell'uso.
        /// </summary>
        private System.Data.Odbc.OdbcConnection MyConn = null;

        #region COSTRUTTORI
        /// <summary>
        /// Dummy Constructor
        /// </summary>
        internal SQLForeignKey()
        {
            // DUMMMY CONSTRUCTOR
        }

        /// <summary>
        /// <p>
        /// Costruttore mediante connessione attiva
        /// </p>
        /// <br>
        /// Di scarsa utilità.
        /// </br>
        /// <pre>
        /// ----
        /// @MdN
        /// Crtd: 02/05/2015
        /// </pre>
        /// </summary>
        /// <param name="p_conn">Connessione attiva</param>
        private SQLForeignKey(OdbcConnection p_conn)
        {
            if (p_conn == null)
                return;
            if (p_conn.State == System.Data.ConnectionState.Open)
                p_conn.Close();
            MyConn = p_conn;
        }

        /// <summary>
        /// <p>
        /// Costruttore mediante tabella referente.
        /// La tabella referente fornisce la connessione.
        /// </p>
        /// <pre>
        /// ----
        /// @MdN
        /// Crtd: 02/05/2015
        /// </pre>
        /// </summary>
        /// <param name="p_table">tabella referente</param>
        internal SQLForeignKey(SQLSrv.SQLTable p_table)
        {
            if (p_table != null)
            {
                MyReferencing = p_table;
                MyConn = p_table.CurrentConnection;
                if (MyConn !=null)
                {
                    if (MyConn.State == System.Data.ConnectionState.Open)
                        MyConn.Close();
                }
            }
        } //fine
        #endregion

        #region metodi privati e interni
        /// <summary>
        /// <strong>FONDAMENTALE!!!</strong>
        /// <p>
        /// Compie le seguenti operazioni
        /// <ol>
        /// <li>Verifica l'esistenza di un vincolo di integrità referenziale per la colonna specificata;</li>
        /// <li>Estrae i dati identificativi del vincolo: ID e Nome</li>
        /// <li>Restituisce il numero di colonne di cui è costituito il vincolo</li>
        /// </ol>
        /// </p>
        /// <p>
        /// Se il vincolo non viene trovato, restituisce 0.
        /// </p>
        /// </summary>
        /// <param name="p_ColumnName">Nome della colonna su cui si vuol indagare ull'esistenza 
        /// di un vincolo di integrità referenziale.</param>
        /// <param name="p_table">tabella di indagine.</param>
        /// <returns>Il numero di colonne di cui è costituito il vincolo ovvero 0 nel caso di nessun vincolo trovato.</returns>
        /// <remarks>Lancia eccezioni di tipo simplex_ORM.Spx_ORMMessage.</remarks>
        internal Boolean load(SQLSrv.SQLTable p_table, String p_ColumnName)
        {
            MyReferencing = p_table;
            return load(p_ColumnName);
        }

        /// <summary>
        /// <strong>FONDAMENTALE!!!</strong>
        /// <p>
        /// Compie le seguenti operazioni
        /// <ol>
        /// <li>Verifica l'esistenza di un vincolo di integrità referenziale per la colonna specificata;</li>
        /// <li>Estrae i dati identificativi del vincolo: ID e Nome</li>
        /// </ol>
        /// </p>
        /// <p>
        /// Se il vincolo non viene trovato, restituisce 0.
        /// </p>
        /// <pre>
        /// ----
        /// @MdN
        /// Crtd: 02/05/2015
        /// Tstd: 02/05/2015
        /// </pre>
        /// </summary>
        /// <param name="p_ColumnName">Nome della colonna su cui si vuol indagare ull'esistenza 
        /// di un vincolo di integrità referenziale.</param>
        /// <returns>Il numero di colonne di cui è costituito il vincolo ovvero 0 nel caso di nessun vincolo trovato.</returns>
        /// <remarks>Lancia eccezioni di tipo simplex_ORM.Spx_ORMMessage.</remarks>
        private int loadID(String p_ColumnName)
        {
            String _sql = null;
            StringBuilder _sb = null;
            OdbcCommand _cmd = null;
            OdbcDataReader _rs = null;
            int _toRet = 0;
            //String _ReferencedTableName = null;
            Int32 _fKeyId = 0;
            String _fKeyName = null;

            
            //controlli
            if (MyConn == null)
                return 0;

            //Apertura
            if (MyConn.State == System.Data.ConnectionState.Closed)
                MyConn.Open();

            _sql = @"SELECT SO.ID
                    , SO.Name as TABELLA
                    , SCO.Name as COLONNA
                    , SFK.fkey as COLONNA_ID
                    , SO.type
                    , SO2.ID AS ID_VINCOLO
                    , SO2.Name AS VINCOLO
                    , SO2.type
                    , SO3.NAME as REFERENCED_TABLE
                    , SCO2.Name as REFERENCED_COLUMN
                    , SFK.rkey AS REFERENCED_COLUMN_ID 
                    , SFK.keyno FROM sysobjects SO
                    LEFT JOIN sysobjects SO2 ON SO2.parent_obj = SO.ID
                    LEFT JOIN sysconstraints SC ON SC.constid=SO2.id
                    LEFT JOIN sysforeignkeys SFK on SFK.constid = SO2.ID
                    LEFT JOIN sysobjects SO3 ON SFK.rkeyid=SO3.id
                    LEFT JOIN syscolumns SCO on SCO.colid=SFK.fkey AND SCO.id=SFK.fkeyid
                    LEFT JOIN syscolumns SCO2 ON SCO2.colid = SFK.rkey and SCO2.id=SFK.rkeyid";
            _sb = new StringBuilder(_sql).Append(" WHERE SO.Name='").Append(MyReferencing.Name).Append("' AND SO2.TYPE='F' AND SCO.Name='").Append(p_ColumnName).Append("'");
            try
            {
                _cmd = MyConn.CreateCommand();
                _cmd.CommandText = _sb.ToString();
                _rs = _cmd.ExecuteReader();

            }
            catch(Exception E)
            {
                throw new simplex_ORM.Spx_ORMMessage(_sb.ToString(), 11, E); 
            }

            try
            {
                if (_rs.HasRows == false)
                {
                    _rs.Close();
                    MyConn.Close();
                    return 0;
                }
                while (_rs.Read())
                {
                    if (_toRet == 0)
                    {
                        //_ReferencedTableName = _rs.GetString(_rs.GetOrdinal("REFERENCED_TABLE"));
                        _fKeyId = _rs.GetInt32(_rs.GetOrdinal("ID_VINCOLO"));
                        _fKeyName = _rs.GetString(_rs.GetOrdinal("VINCOLO"));
                    }
                    _toRet++;
                }
            }
            catch (Exception E)
            {
                throw new simplex_ORM.Spx_ORMMessage("Impossibile ricavare informazioni sul vincolo", 8, E);
            }
            _rs.Close();
            //MyConn.Close();
            if (_fKeyName != null)
            {
                MyID = _fKeyId;
                MyName = _fKeyName;
            }
            else
                _toRet = 0;
            MyConn.Close();
            return _toRet;
        }

        /// <summary>
        /// <strong>FONDAMENTALE!!!</strong>
        /// <p>
        /// Carica tutti i dati identificativi (<see cref="loadID"/>) e costitutivi
        /// del vincolo di integrità referenziale che coinvolge la colonna specificata
        /// come argomento.
        /// </p>
        /// <p>
        /// Se non trova alcun vincolo di integrità referenziale, il metodo restituisce false.
        /// </p>
        /// <pre>
        /// ----
        /// @MdN
        /// Crtd: 02/05/2015
        /// Tstd: 02/05/2015
        /// Mdfd: 11/10/2017 - corretto BUG sulla chiusura della connessione.
        /// </pre>
        /// </summary>
        /// <param name="p_ColumnName">Nome della colonna della tabella referente.</param>
        /// <returns>True, se l'indagine ha esito positivo, False altrimenti.</returns>
        internal Boolean load(String p_ColumnName)
        {

            String _sql = null;
            StringBuilder _sb = null;
            OdbcCommand _cmd = null;
            OdbcDataReader _rs = null;
            int _toRet = 0;
            String _ReferencedTableName = null;
            System.Collections.Specialized.StringCollection _referenced = null;
            System.Collections.Specialized.StringCollection _referencing = null;
            
            int _numCols = 0;           //Numero di colonne coinvolte nel vincolo
            
            //controlli
            if (MyConn == null)
                return false;

            //La colonna specificata è parte della tabella?
            foreach (String _str in MyReferencing.SQLColumns)
            {
                if (_str.ToLower().CompareTo(p_ColumnName.ToLower()) == 0)
                {
                    _toRet = 1;
                    break;
                }
            }
            if (_toRet == 0)
            {
                throw new simplex_ORM.Spx_ORMMessage("La tabella " + MyReferencing.Name + " non ha la colonna " + p_ColumnName + ".",7);
            }

            _toRet=0;   //RESET

            //Apertura
            if (MyConn.State == System.Data.ConnectionState.Closed)
                MyConn.Open();

            // In pratica il metodo lancia la medesima query di interrogazione del 
            // catalogo del database due volte.

            // 1) Carico gli elementi identificativi del vincolo
            _numCols = loadID(p_ColumnName);
            if (_numCols == 0)
            {
                MyConn.Close();
                return false;
            }
            // 2) Carico gli elementi costitutivi del vincolo (le colonne)
            MyNumCols = _numCols;

            //Apertura
            if (MyConn.State == System.Data.ConnectionState.Closed)
                MyConn.Open();

            _sql = @"SELECT SO.ID
                    , SO.Name as TABELLA
                    , SCO.Name as COLONNA
                    , SFK.fkey as COLONNA_ID
                    , SO.type
                    , SO2.ID AS ID_VINCOLO
                    , SO2.Name AS VINCOLO
                    , SO2.type
                    , SO3.NAME as REFERENCED_TABLE
                    , SCO2.Name as REFERENCED_COLUMN
                    , SFK.rkey AS REFERENCED_COLUMN_ID 
                    , SFK.keyno FROM sysobjects SO
                    LEFT JOIN sysobjects SO2 ON SO2.parent_obj = SO.ID
                    LEFT JOIN sysconstraints SC ON SC.constid=SO2.id
                    LEFT JOIN sysforeignkeys SFK on SFK.constid = SO2.ID
                    LEFT JOIN sysobjects SO3 ON SFK.rkeyid=SO3.id
                    LEFT JOIN syscolumns SCO on SCO.colid=SFK.fkey AND SCO.id=SFK.fkeyid
                    LEFT JOIN syscolumns SCO2 ON SCO2.colid = SFK.rkey and SCO2.id=SFK.rkeyid";
            _sb = new StringBuilder(_sql).Append(" WHERE SO2.Name='").Append(MyName).Append("' ORDER BY keyno");
            try
            {
                _cmd = MyConn.CreateCommand();
                _cmd.CommandText = _sb.ToString();
                _rs = _cmd.ExecuteReader(System.Data.CommandBehavior.SingleRow);

            }
            catch (Exception E)
            {
                throw new simplex_ORM.Spx_ORMMessage(_sb.ToString(), 11, E);
            }

            try
            {
                if (_rs.HasRows == false)
                {
                    _rs.Close();
                    MyConn.Close();
                    return false;
                }
                _referenced = new System.Collections.Specialized.StringCollection();
                _referencing = new System.Collections.Specialized.StringCollection();
                while (_rs.Read())
                {
                    if (_toRet == 0)
                    {
                        _ReferencedTableName = _rs.GetString(_rs.GetOrdinal("REFERENCED_TABLE"));
                        if (_ReferencedTableName == null)
                        {
                            _rs.Close();
                            MyConn.Close();
                            return false;
                        }
                    }
                    _referenced.Add(_rs.GetString(_rs.GetOrdinal("REFERENCED_COLUMN")));
                    _referencing.Add(_rs.GetString(_rs.GetOrdinal("COLONNA")));
                    _toRet++;
                }
            }
            catch (Exception E)
            {
                // <added @MdN: 11/10/2017>
                simplex_ORM.Spx_ORMMessage _ormm = new Spx_ORMMessage();
                _ormm.InnerMessage = E;
                _ormm.Message = "Impossibile ricavare informazioni sul vincolo: " + E.Message;
                _ormm.Source = "SQLForeignKey";
                _ormm.MessageCode = 8;
                throw _ormm;
                //throw new simplex_ORM.Spx_ORMMessage("Impossibile ricavare informazioni sul vincolo", 8, E);
                // <added @MdN: 11/10/2017>
            }
            finally
            {
                // <added @MdN: 11/10/2017>
                _rs.Close();
                if (MyConn.State == System.Data.ConnectionState.Open)
                    MyConn.Close();
                // <added @MdN: 11/10/2017>
            }
            MyNumCols = _toRet;
            MyReferenced = simplex_ORM.SQLSrv.SQLTable.createSQLTable(MyConn, _ReferencedTableName);
            MyReferencingKeys = new String[_referencing.Count];
            MyReferencedKeys = new String[_referenced.Count];
            _referencing.CopyTo(MyReferencingKeys, 0);
            _referenced.CopyTo(MyReferencedKeys, 0);
            //_rs.Close(); //<moved @MdN: 11/10/2017>
            // MyConn.Close(); //<moved @MdN: 11/10/2017>
            return true;
        }//FINE LOAD
        #endregion

        #region proprietà pubbliche
        /// <summary>
        /// <p>
        /// Restituisce la tabella referenziata.
        /// </p>
        /// <pre>
        /// ---
        /// @MdN
        /// Crtd:02/05/2015
        /// </pre>
        /// </summary>
        public SQLTable ReferencedTable
        {
            get
            {
                return MyReferenced;
            }
        }

        /// <summary>
        /// <p>
        /// Restituisce la tabella referente.
        /// </p>
        /// <pre>
        /// ---
        /// @MdN
        /// Crtd:02/05/2015
        /// </pre>
        /// </summary>
        public SQLTable ReferencingTable
        {
            get
            {
                return MyReferencing;
            }
        }

        /// <summary>
        /// <p>
        /// Restituisce il numero di colonne coinvolte nel vincolo.
        /// </p>
        /// <pre>
        /// ---
        /// @MdN
        /// Crtd:02/05/2015
        /// </pre>
        /// </summary>
        public int ColumnsCount
        {
            get
            {
                return MyNumCols;
            }
        }

        /// <summary>
        /// <p>
        /// Restituisce il nome del vincolo.
        /// </p>
        /// <pre>
        /// ---
        /// @MdN
        /// Crtd:02/05/2015
        /// </pre>
        /// </summary>
        public String ConstraintName
        {
            get
            {
                return MyName;
            }
        }

        /// <summary>
        /// <p>
        /// Restituisce l'elenco delle colonne della tabella referente
        /// coinvolte nel vincolo.
        /// </p>
        /// <pre>
        /// ---
        /// @MdN
        /// Crtd:02/05/2015
        /// </pre>
        /// </summary>
        public String[] ReferencingColumns
        {
            get
            {
                return MyReferencingKeys;
            }
        }

        /// <summary>
        /// <p>
        /// Restituisce l'elenco delle colonne della tabella referenziata
        /// coinvolte nel vincolo.
        /// </p>
        /// <pre>
        /// ---
        /// @MdN
        /// Crtd:02/05/2015
        /// </pre>
        /// </summary>
        public String[] ReferencedColumns
        {
            get
            {
                return MyReferencedKeys;
            }
        }

        /// <summary>
        /// <p>
        /// Restituisce l'ID del vncolo ricavato da sysobjects.
        /// </p>
        /// <pre>
        /// ---
        /// @MdN
        /// Crtd:02/05/2015
        /// </pre>
        /// </summary>
        public Int32 ConstraintID
        {
            get
            {
                return MyID;
            }
        }




        #endregion

    }
}
