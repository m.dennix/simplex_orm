﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Globalization;     //<-- CultureInfo

namespace simplex_ORM
{
    /// <summary>
    /// <p>
    /// Classe che rappresenta una colonna di una tabella.
    /// </p>
    /// <pre>
    /// ----
    /// @MdN
    /// Crtd: 14/01/2013
    /// Mdfd: 01/05/2015 - resa pubblica Column_Types
    /// Mdfd: 19/10/2015 - correzione BUG sui valori BIT --> 
    /// Mdfd: 15/11/2015 - Corretto il bug sui numeri negativi nella funzione IsNumber
    /// Mdfd: 14/07/2018 - Monterotondo: introdotta property virtual IsIdentity (simplex_ORM.dll 1.1.0.9)
    /// Mdfd: 31/01/2020 - isSQLDateString() modificato il comportamento in modo che riconosca i millisecondi ffff (simplex_ORM.dll 1.1.0.11). 
    /// Mdfd: 09/04/2020 - introdotto attributo l_NBytesColonna e l'attibuto booleano l_isTruncated (simplex_ORM.dll 1.1.0.12)
    ///                  - Modificato il metodo simplex_ORM.Column.SQLDateStringToDate(): deve accettare anche una stringa con i millisecondi.
    /// </pre>
    /// </summary>
    public class Column: GenericObject
    {
        /// <summary>
        /// <p>
        /// Enumerazione dei tipi più comunemente usati dai database, quali:.
        /// </p>
        /// <br>CHARACTER, CHARACTER VARYING, BIT, BIT VARYING, NUMERIC, DECIMAL,</br>
        /// <br>INTEGER, SMALLINT, FLOAT, REAL, DOUBLE PRECISION, DATE, TIME,</br>
        /// <br>TIMESTAMP, and INTERVAL.</br>
        /// In particolare:
        /// <ul>
        /// <li>CHAR</li>
        /// <li>VARCHAR</li>
        /// <li>BIT</li>
        /// <li>VARBIT</li>
        /// <li>NUMERIC</li>
        /// <li>DECIMAL</li>
        /// <li>INTEGER</li>
        /// <li>SMALLINT</li>
        /// <li>BIGINT</li>
        /// <li>FLOAT</li>
        /// <li>REAL</li>
        /// <li>DOUBLE</li>
        /// <li>TIME</li>
        /// <li>DATE</li>
        /// <li>DATETIME</li>
        /// <li>TIMESTAMP</li>
        /// <li>BINARY</li>
        /// <li>ROWVERSION</li>
        /// <li>UNKNOWN</li>
        /// </ul>
        /// </summary>
        public enum Column_Types {
            /// <summary>
            /// CHAR
            /// </summary>
            CHAR, 
            /// <summary>
            /// VARCHAR
            /// </summary>
            VARCHAR,
            /// <summary>
            /// BIT
            /// </summary>
            BIT, 
            /// <summary>
            /// VARBIT
            /// </summary>
            VARBIT,
            /// <summary>
            /// NUMERIC
            /// </summary>
            NUMERIC, 
            /// <summary>
            /// DECIMAL
            /// </summary>
            DECIMAL, 
            /// <summary>
            /// INTEGER
            /// </summary>
            INTEGER,
            /// <summary>
            /// SMALLINT
            /// </summary>
            SMALLINT, 
            /// <summary>
            /// BIGINT
            /// </summary>
            BIGINT, 
            /// <summary>
            /// FLOAT
            /// </summary>
            FLOAT, 
            /// <summary>
            /// REAL
            /// </summary>
            REAL, 
            /// <summary>
            /// DOUBLE
            /// </summary>
            DOUBLE, 
            /// <summary>
            /// DATE
            /// </summary>
            DATE, 
            /// <summary>
            /// TIME
            /// </summary>
            TIME, 
            /// <summary>
            /// DATETIME
            /// </summary>
            DATETIME, 
            /// <summary>
            /// TIMESTAMP
            /// </summary>
            TIMESTAMP, 
            /// <summary>
            /// BINARY
            /// </summary>
            BINARY, 
            /// <summary>
            /// ROWVERSION
            /// </summary>
            ROWVERSION, 
            /// <summary>
            /// UNKNOWN
            /// </summary>
            UNKNOWN};

        #region ATTRIBUTI
        /// <summary>
        /// Nome della colonna
        /// </summary>
        internal String l_Name;
        /// <summary>
        /// Valore della colonna
        /// </summary>
        internal String l_Value;
        /// <summary>
        /// Tipo SQL del valore della colonna
        /// </summary>
        internal int l_SQLType;
        /// <summary>
        /// FLAG: la colonna è una chiave primaria
        /// </summary>
        internal Boolean l_IsPrimayKey;
        /// <summary>
        /// FLAG: la colonna è parte di una chiave primaria composita
        /// </summary>
        internal Boolean l_IsKeyPart;
        /// <summary>
        /// Numero di colonne che partecipano ad una chiave primaria composita
        /// </summary>
        internal int l_KeyCount;
        /// <summary>
        /// FLAG: la colonna ha un valore autoincrmentate (IDENTITY).
        /// </summary>
        internal Boolean l_IsIdentity;
        /// <summary>
        /// FLAG: la colonna ammette valori nulli.
        /// </summary>
        internal Boolean l_IsNullable;
        /// <summary>
        /// FLAG: la colonna ammette un valore di default.
        /// </summary>
        internal Boolean l_HasDefault;
        /// <summary>
        /// Riferimento all'ultima colonna di una chiave composta.
        /// </summary>
        internal Column l_LastKeyPart;
        /// <summary>
        /// FLAG: il valore della colonna è stato modificato dall'ultimo caricamento.
        /// </summary>
        internal Boolean l_IsModified;
        /// <summary>
        /// Valore della colonna precedente all'ultima modifica.
        /// </summary>
        internal String l_OldValue;
        /// <summary>
        /// Dimensione massima in byte dei valori della colonna.<br></br>
        /// <pre>
        /// ----
        /// @MdN Crtd: 09/04/2020 - v.1.1.0.4 (milestone 1.1.0.12)
        /// </pre>
        /// </summary>
        internal int l_NBytesColonna;
        /// <summary>
        /// Indica se il valore di una data colonna "stringa di testo" è stata troncata, cioè limitata nella
        /// sua dimensione al massimo valore di byte definito per quella colonna.<br></br>
        /// <pre>
        /// ----
        /// @MdN Crtd: 09/04/2020 - v.1.1.0.4 (milestone 1.1.0.12)
        /// </pre>
        /// </summary>
        internal Boolean l_isTruncated = false;
        #endregion

        #region STATIC
        /// <summary>
        /// Dato un valore Datetime, restituisce la corrispondente stringa col seguente formato SQL:
        /// "YYYY-MM-DD[spazio]hh:mm:ss"
        /// @MdN - 10/01/2014
        /// </summary>
        /// <param name="p_date">Data da trasformare</param>
        /// <returns>stringa di formato "YYYY-MM-DD[spazio]hh:mm:ss"</returns>
        public static String DateToSQLDateString(DateTime p_date)
        {
            //attenzione al carattere separatore del tempo: 
            //System.Globalization.DateTimeFormatInfo.CurrentInfo.TimeSeparator;
            if(p_date==null) //@MdN 16/11/2014
                return  DateTime.Today.ToString("yyyy-MM-dd HH:mm:ss").Replace(System.Globalization.DateTimeFormatInfo.CurrentInfo.TimeSeparator, ":");
            return p_date.ToString("yyyy-MM-dd HH:mm:ss").Replace(System.Globalization.DateTimeFormatInfo.CurrentInfo.TimeSeparator, ":");
        }

        /// <summary>
        /// Decide se una stringa è un numero o meno.
        /// @MdN 14/01/2014
        /// <pre>
        /// ----
        /// @MdN
        /// Crtd: 14/01/2014
        /// Mdfd: 15/11/2015 - Corretto il bug sui numeri negativi
        /// </pre>
        /// </summary>
        /// <param name="p_Number">Stringa da riconoscere.</param>
        /// <returns>TRUE se p_Number è un numero, false altrimenti.</returns>
        public static Boolean isNumber(String p_Number)
        {
            char l_x = '0';
            char l_dot = '.';
            char l_comma = ',';            
            char[] _toTrim = { '-','+', (char)32, '\t', '\r', '\n' };     //@MdN 15/11/2015

            // CONTROLLO PARAMETRI @MdN 16/11/2014
            if (p_Number == null)
                return false;
            Boolean toRet=true;

            p_Number = p_Number.Trim(_toTrim);                             //@MdN 15/11/2015

            for (int t = 0; t < p_Number.Length; t++)
            {
                if ((p_Number[t] >= l_x && p_Number[t] <= l_x + 9) || p_Number[t] == l_dot || p_Number[t] == l_comma)
                    toRet = toRet & true;
                else
                {
                    toRet = toRet & false;
                    break;
                }
            }
            return toRet;
        }

        /// <summary>
        /// Decide se una stringa è un numero decimale o meno.
        /// @MdN 01/03/2014 - 16/11/2014
        /// </summary>
        /// <param name="p_Number">Stringa da riconoscere</param>
        /// <param name="p_Separator">Separatore</param>
        /// <returns>TRUE se p_Number è un numero decimale, false altrimenti.</returns>
        public static Boolean isDecimal(String p_Number, char p_Separator)
        {
            if (p_Number == null)
                return false;
            //<deleted @MdN 25/01/2020>
            //if (p_Separator == null)
            //    return false;
            //<deleted @MdN 25/01/2020>
            return (isNumber(p_Number)& p_Number.Contains(p_Separator));
        }

        /// <summary>
        /// IT: Pulisce la stringa passata per parameto dal carattere specificato.
        /// EN: Clears the string passed as parameter from the specified char.
        /// <pre>
        /// ----
        /// @MdN
        /// Crtd: 18/11/2014
        /// Mdfd: 01/10/2017 - Monterotondo: corretto BUG (documentato in TODO il 21/09/2017).  
        /// </pre>
        /// </summary>
        /// <param name="p_toClear">Stringa da pulire / String to clear</param>
        /// <param name="p_char">Carattere da rimuovere / Char to remove</param>
        /// <returns>the cleared string</returns>
        public static String clearString(String p_toClear, char p_char)
        {
            String[] _StrArray = null;
            Char[] _Separator = new char[1];
            StringBuilder _sb = null;
            
            _Separator[0]=p_char;

            if(p_toClear==null)
                return null;
            if(p_char=='\0')
                return null;
            _StrArray = p_toClear.Split(_Separator);
            _sb = new StringBuilder();
            foreach (String _s in _StrArray)
                _sb.Append(_s);
            return(_sb.ToString());
        } // fine funzione
        

        /// <summary>
        /// Verifica se nella stringa passata per argomento vi è una ripetizione continuata di un carattere X.<br></br>
        /// </summary>
        /// <param name="p_TragetChar">Il carattere di indagine.</param>
        /// <param name="p_String">Stringa di indagine.</param>
        /// <param name="p_start">output: posizione iniziale della sequenza ripetitiva del carattere di indagine.</param>
        /// <param name="p_end">output: posizione finale della sequenza ripetitiva del carattere di indagine.</param>
        /// <returns>True se il carattere è presente in sequenza continua e ripetitiva.</returns>
        private static Boolean isPresentContiguously(char p_TragetChar, String p_String, out int p_start, out int p_end)
        {
            p_start = 0;
            p_end = 0;
            //<deleted @MdN 25/01/2020>
            //if (p_TragetChar == null || p_String == null)
            //    return false;
            //</deleted @MdN 25/01/2020>
            //<added @MdN 25/01/2020>
            if (p_String == null)
                return false;
            //</added @MdN 25/01/2020>


            int t = 0;
            char[] _String;

            t = p_String.IndexOf(p_TragetChar);                   // PRENDE LA PRIMA OCCORRENZA DEL CARATTERE DI RIFERIMENTO
            if (t >= 0 && t < p_String.Length)                    // Ciclo di ricerca della sequenza del carattere di riferimento.
            {
                _String = p_String.ToCharArray();
                p_start = t;
                p_end = t;
                while (p_end < _String.Length)
                {
                    if (_String[p_end] == p_TragetChar)
                        p_end++;
                    else
                        break;
                }
            }
            else
                return false;
            // OK, trovato!
            return true;
        }// fine funzione

        /// <summary>
        /// <p>
        /// Check if a number is an integer without any separator cher.
        /// </p>
        /// <pre>
        /// ----
        /// @MdN 
        /// Crtd: 14/03/2015 
        /// </pre>
        /// </summary>
        /// <param name="p_Number">A string</param>
        /// <returns>TRUE if p_number is a string with only NUMBERS [0-9] and without any separator char like '.' or ','.</returns>
        public static Boolean isPureInteger(String p_Number)
        {
            char l_x = '0';
            // CONTROLLO PARAMETRI @MdN 16/11/2014
            if (p_Number == null)
                return false;
            Boolean toRet = true;
            for (int t = 0; t < p_Number.Length; t++)
            {
                if ((p_Number[t] >= l_x && p_Number[t] <= l_x + 9))
                    toRet = toRet & true;
                else
                {
                    toRet = toRet & false;
                    break;
                }
            }
            return toRet;
        }

        /// <summary>
        /// [IT]Verifica se una stringa rappresenta una data in un formato specificato.
        /// D: giorno<br></br>
        /// M: mese<br></br>
        /// Y: anno<br></br>
        /// h: hours<br></br>
        /// m: minuti<br></br>
        /// s: secondi<br></br>
        /// d: decimi<br></br>
        /// <br></br>
        /// [EN]Check if a string is a datetime of the specified format.
        /// D: day<br></br>
        /// M: month<br></br>
        /// Y: year<br></br>
        /// h: hours<br></br>
        /// m: minutes<br></br>
        /// s: seconds<br></br>
        /// d: decimal<br></br>
        /// <pre>
        /// ----
        /// @MdN
        /// Crtd: 17/11/2014        
        /// Mdfd: 14/03/2015
        /// Mdfd: 12/07/2015 - Controllo valore nullo del parametro.
        /// </pre>
        /// </summary>
        /// <param name="p_date">Date in string format</param>
        /// <param name="p_format">format string</param>
        /// <returns></returns>
        public static Boolean isDateTime(String p_date, String p_format)
        {
            //int _day;
            //int _month;
            //int _year;
            //int _hours;
            //int _minutes;
            //int _seconds;
            //int _decimals;
            //---------------
            int _start = 0;
            int _end = 0;
            String _appoggio = null;
            Boolean toRet = true;
            Boolean toRetII = false;
            //String _separator = null; //<deleted @MdN 25/01/2020 />

            //12/07/2015 - Controllo valore nullo del parametro.
            if (p_date == null)
                return false;
            //12/07/2015 - Controllo valore nullo del parametro.
            if (p_format == null)
                return false;

            //Controllo delle lunghezza: 14/03/2015
            if(p_format.Length != p_date.Trim().Length)
                return false;

            try
            {
                    //YEAR
                    if (Column.isPresentContiguously('Y', p_format, out _start, out _end) == true)
                    {
                        _appoggio = p_date.Substring(_start, _end - _start);
                        toRet = toRet & Column.isPureInteger(_appoggio);
                        toRetII = true;
                    }
                    // MONTH
                    _start = 0;
                    _end = 0;
                    _appoggio = null;
                    if (Column.isPresentContiguously('M', p_format, out _start, out _end) == true)
                    {
                        _appoggio = p_date.Substring(_start, _end - _start);
                        if (Column.isNumber(_appoggio))
                        {
                            if (Int16.Parse(_appoggio) > 0 && Int16.Parse(_appoggio) <= 12)
                            {
                                toRet = toRet & Column.isPureInteger(_appoggio);
                                toRetII = true;
                            }
                        }
                        else
                            return false;
                    }
                    //DAY
                    _start = 0;
                    _end = 0;
                    _appoggio = null;
                    if (Column.isPresentContiguously('D', p_format, out _start, out _end) == true)
                    {
                        _appoggio = p_date.Substring(_start, _end - _start);
                        if (Column.isPureInteger(_appoggio))
                        {
                            if (Int16.Parse(_appoggio) > 0 && Int16.Parse(_appoggio) <= 31)
                            {
                                toRet = toRet & Column.isNumber(_appoggio);
                                toRetII = true;
                            }
                        }
                        else
                            return false;
                    }
                    //HOUR
                    _start = 0;
                    _end = 0;
                    _appoggio = null;
                    if (Column.isPresentContiguously('h', p_format, out _start, out _end) == true)
                    {
                        _appoggio = p_date.Substring(_start, _end - _start);
                        if (Column.isPureInteger(_appoggio))
                        {
                            if (Int16.Parse(_appoggio) > 0 && Int16.Parse(_appoggio) <= 24)
                            {
                                toRet = toRet & Column.isNumber(_appoggio);
                                toRetII = true;
                            }
                        }
                        else
                            return false;
                    }
                    //MINUTE
                    _start = 0;
                    _end = 0;
                    _appoggio = null;
                    if (Column.isPresentContiguously('m', p_format, out _start, out _end) == true)
                    {
                        _appoggio = p_date.Substring(_start, _end - _start);
                        if (Column.isNumber(_appoggio))
                        {
                            if (Int16.Parse(_appoggio) > 0 && Int16.Parse(_appoggio) <= 60)
                            {
                                toRet = toRet & Column.isPureInteger(_appoggio);
                                toRetII = true;
                            }
                        }
                        else
                            return false;
                    }
                    //SECOND
                    _start = 0;
                    _end = 0;
                    _appoggio = null;
                    if (Column.isPresentContiguously('s', p_format, out _start, out _end) == true)
                    {
                        _appoggio = p_date.Substring(_start, _end - _start);
                        if (Column.isPureInteger(_appoggio))
                        {
                            if (Int16.Parse(_appoggio) > 0 && Int16.Parse(_appoggio) <= 60)
                            {
                                toRet = toRet & Column.isNumber(_appoggio);
                                toRetII = true;
                            }
                        }
                        else
                            return false;
                    }
                    //DECIMAL
                    _start = 0;
                    _end = 0;
                    _appoggio = null;
                    if (Column.isPresentContiguously('d', p_format, out _start, out _end) == true)
                    {
                        _appoggio = p_date.Substring(_start, _end - _start);
                        if (Column.isPureInteger(_appoggio))
                        {
                            if (Int16.Parse(_appoggio) > 0 && Int16.Parse(_appoggio) < 1000)
                            {
                                toRet = toRet & Column.isNumber(_appoggio);
                                toRetII = true;
                            }
                        }
                        else
                            return false;
                    }

                    // Ciclo di controllo separatori
                    // @MdN 14/03/2015
                    Char _c;
                    Int16 _t = 0;
                    Char[] _ch_format= p_format.ToCharArray();
                    Char[] _ch_data=p_date.ToCharArray();
                    for (_t = 0; _t < p_format.Length; _t++)
                    {
                        switch(_c=_ch_format[_t])
                        {
                            case 'Y':
                            case 'M':
                            case 'D':
                            case 'h':
                            case 'm':
                            case 's':
                            case 'd':
                                {
                                    if (_ch_data[_t] <= '9' && _ch_data[_t] >= '0')
                                        toRet = toRet & true;
                                    else
                                        toRet = false;
                                }
                                break;
                            default:
                                {
                                    /* IL CARATTERE DEVE ESSERE UGUALE AL SEPARATORE */
                                    if (_c == _ch_data[_t])
                                        toRet = toRet & true;
                                    else
                                        toRet = false;
                                }
                                break;
                        }
                    }
                    return toRet & toRetII;
            }
            catch (Exception)
            {
                return false;
            }
        }//fine

        /// <summary>
        /// Decide se una stringa è un numero decimale o meno.
        /// IL SEPARATORE DI DEFAULT è IL CARATTERE VIRGOLA ','
        /// @MdN 01/03/2014  
        /// </summary>
        /// <param name="p_Number">Stringa da riconoscere</param>
        /// <returns>TRUE se p_Number è un numero decimale, false altrimenti.</returns>
        public static Boolean isDecimal(String p_Number)
        {
            if (p_Number == null)
                return false;
            return(isDecimal(p_Number,','));
        }

        /// <summary>
        /// D: day
        /// M: month
        /// Y: year
        /// h: hours
        /// m: minutes
        /// s: seconds
        /// d: decimal
        /// </summary>
        /// <param name="p_toParse"></param>
        /// <param name="p_format"></param>
        /// <returns></returns>
        public static DateTime parseDateTimeString(String p_toParse, String p_format)
        {
            ////Posizioni dei componenti
            //int DPos, MPos, YPos, hPos, mPos, sPos, dPos;
            //// caratteri Separatori: seguono il componente
            //char DSep, MSep, YSep, hSep, mSep, sSep, dSep;
            //// Posizioni dei separatori
            //int DSepPos, MSepPos, YSepPos, hSepPos, mSepPos, sSepPos, dSepPos;

            // Variabili:
            // Simboli
            char[] Symbols = new char[7]{'D', 'M', 'Y', 'h', 'm', 's', 'd'};
            // Posizioni
            int[] Pos = new int[7];
            // caratteri separatori
            char[] Sep = new char[7];
            // Posizioni dei separatori (se stringhe, posizione del primo carattere)
            int[] PosSep = new int[7];
            // Stringhe da interpretare
            String[] MyToParse = new String[7];
            // Componenti della data
            int MyDay, MyMonth, MyYear, MyHours, MyMins, MySecs, MyDecs;

            int idx = 0;// Indice

            //CONTROLLI
            if ((p_toParse == null) || (p_format == null))
            {
                Spx_ORMMessage l_mex=new simplex_ORM.Spx_ORMMessage("in parseDateTimeString(): null string to parse.", 1);
                throw l_mex;
            }
            if ((p_toParse.Length == 0) || (p_format.Length == 0))
            {
                Spx_ORMMessage l_mex = new Spx_ORMMessage("in parseDateTimeString(): empty string to parse.", 1);
                throw l_mex;
            }
                //throw new simplex_ORM.Spx_ORMMessage("in parseDateTimeString(): empty string to parse.", 1);

            // **** CICLO PRINCIPALE **** 
            for (idx = 0; idx < 7; idx++)
            {
                Pos[idx] = 0;
                Sep[idx] = ' ';
                PosSep[idx] = 0;

                // Posizione del simbolo nella stringa del formato
                Pos[idx] = p_format.IndexOf(Symbols[idx]);
                if (Pos[idx] == -1)
                {
                    if (idx <= 2)
                    {//Devono essere presenti almeno tutte le componenti della data D, M, Y
                        throw new Spx_ORMMessage("Column.parseDateTimeString(): symbol '" + Symbols[idx] + "' not found.", 0);
                    }
                    else
                    {
                        continue;
                    }
                }

                // A partire dalla posizione iniziale, si cerca nella stringa di formato la lunghezza in caratteri del simbolo.
                int t;
                for (t = Pos[idx]; t <= p_format.Length; t++)
                {
                    // si cerca il simbolo corrente (idx)
                    // La variabile t contiene la lunghezza in caratteri del
                    // simbolo corrente.
                    if (t == p_format.Length)
                    {
                        Sep[idx] = '\n';
                        PosSep[idx] = t;
                        break;
                    }

                    if (p_format[t] != Symbols[idx])
                    {
                        //Fine del simbolo o della sua ripetizione--> Memorizziamo il carattere separatore. 
                        if (p_format[t] != 'D' && p_format[t] != 'M' && p_format[t] != 'Y' && p_format[t] != 'm' && p_format[t] != 'h' && p_format[t] != 's' && p_format[t] != 'd')
                            Sep[idx] = p_format[t];
                        PosSep[idx] = t; // DI CONSEGUENZA la lunghezza in caratteri del simbolo corrente è: PosSep[idx] - Pos[idx]
                        break;
                    }
                }
                //Estrazione del componente e controlli:
                if (Pos[idx] > -1 && PosSep[idx]<=p_toParse.Length)
                {
                    MyToParse[idx] = p_toParse.Substring(Pos[idx], PosSep[idx] - Pos[idx]);
                    // controllo
                    if (MyToParse[idx] != null)
                        if (Column.isNumber(MyToParse[idx])==false)
                            MyToParse[idx] = null;
                }
                else
                    MyToParse[idx] = null;
            }
            /* INTERPRETAZIONE */
            /*Giorno*/
            if (MyToParse[0] != null)
                MyDay = Int16.Parse(MyToParse[0]);
            else
                MyDay = 0;

            /*Mese*/
            if (MyToParse[1] != null)
                MyMonth = Int16.Parse(MyToParse[1]);
            else
                MyMonth = 0;

            /*Year*/
            if (MyToParse[2] != null)
            {
                MyYear = Int16.Parse(MyToParse[2]);
                if (MyYear < 100)
                {
                    //Aggiungo il secolo ed il millennio corrente
                    MyYear = ((DateTime.Now.Year) / 100) * 100 + MyYear;
                }
            }
            else
                MyYear = 0;

            /* hours */
            if (MyToParse[3] != null)
                MyHours = Int16.Parse(MyToParse[3]);
            else
                MyHours = 0;

            /* Minutes */
            if (MyToParse[4] != null)
                MyMins = Int16.Parse(MyToParse[4]);
            else
                MyMins = 0;

            /* Seconds */
            if (MyToParse[5] != null)
                MySecs = Int16.Parse(MyToParse[5]);
            else
                MySecs = 0;

            /* Decimals */
            if (MyToParse[6] != null)
                MyDecs = Int16.Parse(MyToParse[6]);
            else
                MyDecs = 0;
            try
            {
                return new DateTime(MyYear, MyMonth, MyDay, MyHours, MyMins, MySecs, MyDecs);
            }
            catch (Exception E)
            {
                throw new Spx_ORMMessage("Column.parseDateTimeString(): " + E.Message,0,E);
            }
        }

        /// <summary>
        /// Verifica se il formato di una stringa rispetta lo schema SQL92
        /// yyyy-MM-dd HH:mm:ss.ffff
        /// <pre>
        /// ----
        /// @MdN 
        /// Crtd: 14/01/2014
        /// Mdfd: 31/01/2020 - modificato il comportamento in modo che riconosca i millisecondi ffff.
        /// </pre>
        /// </summary>
        /// <param name="p_SQLDateString">Stringa da riconoscere.</param>
        /// <returns>True se la stringa viene riconosciuta, False altrimenti.</returns>
        public static Boolean isSQLDateString(String p_SQLDateString)
        {
            String l_Data;
            String l_Num;
            String l_Millis;                //<added @MdN: 31/01/2020 />

            // CONTROLLI SUGLI ARGOMENTI
            if (p_SQLDateString == null)
                return false;
            //1) Eliminazione degli spazi
            p_SQLDateString.Trim();
            //2) Mi aspetto una stringa di questo formato:yyyy-MM-dd HH:mm:ss.fff
            //Lunghezza

            //if (p_SQLDateString.Length != 19 && p_SQLDateString.Length != 10)
            //    return false;

            if (p_SQLDateString.Length < 10)
            return false;


            //INIZIO DATA
            //Verifica dei primi quattro caratteri
            if (p_SQLDateString.Length >= 10)
            {
                l_Data = p_SQLDateString.Substring(0, 10);
                l_Num = p_SQLDateString.Substring(0, 4);
                if (Column.isNumber(l_Num) == false)
                    return false;

                if (p_SQLDateString[4] != '-')
                    return false;

                l_Num = p_SQLDateString.Substring(5, 2);
                if (Column.isNumber(l_Num) == false)
                    return false;

                if (p_SQLDateString[7] != '-')
                    return false;

                l_Num = p_SQLDateString.Substring(8, 2);
                if (Column.isNumber(l_Num) == false)
                    return false;

                if (p_SQLDateString[10] != ' ')
                    return false;
            }
            
            //FINE DATA
            if (p_SQLDateString.Length == 10)
                return true;
            
            // INIZIO ORARIO
            if (p_SQLDateString.Length >= 19)
            {
                l_Num = p_SQLDateString.Substring(11, 2);
                if (Column.isNumber(l_Num) == false)
                    return false;
                if (p_SQLDateString[13] != ':')
                    return false;

                l_Num = p_SQLDateString.Substring(14, 2);
                if (Column.isNumber(l_Num) == false)
                    return false;
                if (p_SQLDateString[16] != ':')
                    return false;

                l_Num = p_SQLDateString.Substring(17, 2);
                if (Column.isNumber(l_Num) == false)
                    return false;
            }

            if (p_SQLDateString.Length == 19)
                return true;
            else
            {
                if(p_SQLDateString[19]!='.')
                    return false;                
                l_Millis = p_SQLDateString.Substring(20, p_SQLDateString.Length - 20);
                if (l_Millis == null)
                    return false;
                if (Column.isNumber(l_Millis) == false)
                    return false;
                return true;
            }

            // FINE ORARIO
        }
        
        
        /// <summary>
        /// Esegue il parsing di una stringa di formato DATETIME SQL92.
        /// <pre>
        /// @MdN - 14/01/2014
        /// @MdN - 10/04/2020 v.1.1.0.12 - simplex_ORM.Column.SQLDateStringToDate(): deve accettare anche una stringa con i millisecondi.
        /// </pre>
        /// </summary>
        /// <param name="p_SQLDateString"></param>
        /// <returns>Il valore DateTime corrispondente alla stringa passata come parametro.</returns>
        public static DateTime SQLDateStringToDate(String p_SQLDateString)
        {
            Int32 _millis = 0;
            String _milliStr = null;
            //Boolean _hasMillis = false;
            DateTime _toRet;

            if (isSQLDateString(p_SQLDateString) == false)
                throw new Spx_ORMMessage("Column.SQLDateStringToDate(): Wrong format.", 0);

            // Verifichiamo se sono presenti i millisecondi
            if (p_SQLDateString.Length > "yyyy-MM-dd HH:mm:ss".Length)
            {
                //_hasMillis = true;
                int _pos = 0;
                _pos = p_SQLDateString.LastIndexOf('.');
                _milliStr = p_SQLDateString.Substring(_pos+1, p_SQLDateString.Length);
                p_SQLDateString = p_SQLDateString.Substring(0, _pos);
                // il separatore '.' (punto) non dovrebbe più esserci
                int.TryParse(_milliStr, out _millis);
                _toRet = DateTime.ParseExact(p_SQLDateString, "yyyy-MM-dd HH:mm:ss", CultureInfo.InvariantCulture);
                _toRet.AddMilliseconds((double)_millis);
                return _toRet;
            }

            try
            {
                return DateTime.ParseExact(p_SQLDateString, "yyyy-MM-dd HH:mm:ss", CultureInfo.InvariantCulture);
            }
            catch (Exception E)
            {
                throw new Spx_ORMMessage("Column.SQLDateStringToDate(): Unable to parse the SQL Date string.", 0, E);
            }
        }



        #endregion

        #region INTERNAL
        /// <summary>
        /// Tipo SQL dei valori della colonna
        /// </summary>
        internal virtual int SQLType
        {
            get
            {
                return l_SQLType;
            }
        }

        /// <summary>
        /// Legge e scrive il valore di un attributo (è una stringa).
        /// @MdN: 14/01/2013
        /// </summary>
        internal String ColumnValue
        {
            get
            {
                return l_Value;
            }
            set
            {
                l_OldValue = l_Value;
                l_Value = value;
                l_IsModified = true;
            }
        }

        /// <summary>
        /// REstituisce una stringa che rappresenta il valore della colonna, eventualmente tra apici, nel formato opportuno.
        /// E' possibile immettere la stringa direttamente in una query SQL.
        /// ---
        /// @MdN
        /// Crtd:
        /// Mdfd: @MdN 13/12/2014;
        /// </summary>
        internal String SQLColumnValue
        {
            get
            {
                if (l_Value == null)
                    return "NULL";

                switch(this.l_SQLType)
                {
                    case (int)Column.Column_Types.DATE:
                    case (int)Column_Types.DATETIME:
                    {
                        //@MdN: 09/12/2014
                        if (this.l_Value == null)               //@MdN: 09/12/2014
                            return "NULL";
                        if (this.l_Value.Length == 0)           //@MdN: 09/12/2014
                            return "NULL";
                        if(isSQLDateString(this.l_Value)==true)
                            return "CONVERT(DATETIME, '" + l_Value + "', 120)";         //La data DEVE ESSERE già in formato SQL - @MdN 13/12/2014: INSERITA LA CONVERSIONE ESPLICITA CONVERT(DATETIME, ..., 120)
                        else
                            return "NULL";                      //@MdN: 09/12/2014
                    }
                    case (int)Column.Column_Types.CHAR:
                    case (int)Column_Types.VARCHAR:
                    {
                        return "'" + l_Value.Replace("'","''") + "'";
                    }
                    case (int)Column_Types.FLOAT:
                    case (int)Column_Types.DOUBLE:
                    case (int)Column_Types.DECIMAL:
                    case (int)Column_Types.NUMERIC:
                    case (int)Column_Types.REAL:
                    {
                        //@MdN: 09/12/2014
                        if (this.l_Value == null)               //@MdN: 09/12/2014
                            return "NULL";
                        if (this.l_Value.Length == 0)           //@MdN: 09/12/2014
                            return "NULL";

                        return "'" + l_Value.Replace(",", ".") + "'";
                    }
                    case (int)Column_Types.BIT:
                    {
                        // @MdN: 19/10/2015 - correzione BUG
                        if (this.l_Value.ToLower().CompareTo("true") == 0)  // @MdN: 19/10/2015
                            return "1";
                        if (this.l_Value.ToLower().CompareTo("false") == 0) // @MdN: 19/10/2015
                            return "0";
                        return l_Value;                                     // @MdN: 19/10/2015
                    }
                    default:
                        {
                            return l_Value;
                        }
                }
                //return l_Value;
            }
        }

        /// <summary>
        /// Crea un oggetto di tipo Column. <br></br>
        /// L'implementazione base crea una colonna SQL Server.<br></br>
        /// Esegue il mapping SQLServer-.NET.<br></br>
        /// Per creare una colonna per un altro database, eseguire l'override.<br></br>
        /// <pre>
        /// ----
        /// @MdN
        /// Crtd: 14/01/2013
        /// </pre>
        /// </summary>
        /// <param name="p_Name">Nome della colonna.</param>
        /// <param name="p_Type">Tipo SQL della colonna.</param>
        internal Column(String p_Name, int p_Type)
        {

            //Il tipo di default è sempre una stringa.
            if (p_Type == 0)
                l_SQLType = (int)Column_Types.VARCHAR;
            else
                l_SQLType = p_Type;

            //Il nome di default è Field_<numero di millisecondi al momento>
            if (p_Name == null)
                l_Name = "Field_" + System.DateTime.Now.Millisecond.ToString();
            else
                l_Name = p_Name;

            l_HasDefault = false;
            l_IsIdentity = false;
            l_IsKeyPart = false;
            l_IsModified = false;
            l_IsNullable = false;

            l_Value = null;
            l_LastKeyPart = null;
            l_OldValue = null;

            l_KeyCount = 0;
        }

        /// <summary>
        /// Costruttore di default!<br></br>
        /// <pre>
        /// ----
        /// @MdN
        /// Crtd: 14/12/2013
        /// </pre>
        /// </summary>
        internal Column()
        {
            l_Name = null;
            l_SQLType = 0;
            l_Value = null;
            l_LastKeyPart = null;
            l_HasDefault = false;
            l_IsIdentity = false;
            l_IsKeyPart = false;
            l_IsModified = false;
            l_IsNullable = false;
            l_OldValue = null;
        }

        /// <summary>
        /// Esegue il mapping SQL - .NET
        /// MdN: 14/01/2013 - 13/12/2013
        /// L'implementazione base fa riferimento a SQL Server.
        /// Per creare un mapping per un altro database, eseguire l'override.
        /// </summary>
        /// <param name="p_SQLType">Tipo SQL della colonna.</param>
        /// <returns>Restituisce l'oggetto Type corrispondente al tipo SQL passato come parametro.</returns>
        internal virtual Type getColumnType(int p_SQLType)
        {
            Type toRet = null;
            switch (p_SQLType)
            {
                case (int)Column_Types.BIT:
                    {
                        toRet = new Boolean().GetType();
                        break;
                    }
                case (int)Column_Types.CHAR:
                    {
                        toRet = new Char().GetType();
                        break;
                    }
                case (int)Column_Types.DATE:
                case (int)Column_Types.TIME:
                case (int)Column_Types.DATETIME:
                    {
                        toRet = new DateTime().GetType();
                        break;
                    }
                case (int)Column_Types.DECIMAL:
                case (int)Column_Types.NUMERIC:
                case (int)Column_Types.DOUBLE:
                case (int)Column_Types.FLOAT:
                case (int)Column_Types.REAL:
                    {
                        toRet = new Double().GetType();
                        break;
                    }
                case (int)Column_Types.INTEGER:
                    {
                        toRet = new Int32().GetType();
                        break;
                    }
                case (int)Column_Types.SMALLINT:
                    {
                        toRet = new Int16().GetType();
                        break;
                    }
                case (int)Column_Types.BIGINT:
                case (int)Column_Types.TIMESTAMP:
                case (int)Column_Types.ROWVERSION:
                    {
                        toRet = new Int64().GetType();
                        break;
                    }

                case (int)Column_Types.VARBIT:
                case (int)Column_Types.VARCHAR:
                case (int)Column_Types.BINARY:
                    {
                        toRet =  Type.GetType("System.String");
                        break;
                    }
                default:
                    {
                        toRet = Type.GetType("System.String");
                        break;
                    }       
            }
            return toRet;
        }

        /// <summary>
        /// Restituisce (RO) TRUE se la cononna è di tipo Identity.
        /// <pre>
        /// ----
        /// @MdN
        /// Crtd: 14/07/2018 - Monterotondo 
        /// </pre>
        /// </summary>
        internal virtual Boolean IsIdentity
        {
            get
            {
                return l_IsIdentity;
            }
        }
            
        #endregion

    }
}
